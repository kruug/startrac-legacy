

/********************************************************************/
/*				S_SNOTES.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	NOTES to Startrac Pgm			            */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	4-25-97 				    */
/*	LAST UPDATE:	4-25-97  				    */
/*------------------------------------------------------------------*/
/*								    */
/*								    */
/*								    */
/********************************************************************/

#include "window.h"
#include "defaults.h"

void s_snotes() 

{
 int i, pen_color;
 char *snotes[24];

 Window wnotes;
 
 snotes[1] =
 " Upon System Start-Up make sure that TELESCOPE is positioned such that the\n";
 snotes[2] =
 " HOME MARKERS provided on the Azimuth & Elevation Axes & CompuMotor shafts\n";
 snotes[3] = 
 " are Precisely Aligned.  Turn the Telescope Power ON.  Then, using the    \n";
 snotes[4] = 
 " STARTRAC Opening Menu, (1) set the system DATE and TIME, (2) RESET the 2100\n";
 snotes[5] =
 " Indexer, and (3) INITIALIZE the system to the HOME position.          \n\n";
 snotes[6] =
 " [Note: Local TIME should be set to the accuracy provided by NBS Station WWV\n"; 
 snotes[7] =
 "   For this program, CDT is in effect from April 1 to Oct 31, else use CST.]\n";
 snotes[8] =
 "                                                                          \n";
 snotes[9] = 
 " The System should now be aligned accurately enough to place an object    \n";
 snotes[10] =
 " within the CCD Viewport by entering its Right Ascension and Declination  \n";
 snotes[11] =
 " from the MOVE/TRACK Menu.                                              \n";
 snotes[12] =
 "                                                                          \n";
 snotes[13] =
 " Exact Alignment of the Telescope, center pixel of the CCD, and the object\n";
 snotes[14] = 
 " being viewed can easily be made  (from the tracking mode)  by using the \n";
 snotes[15] =
 " Vernier Controls.  Re-initialization of the System from this point will\n";
 snotes[16] =
 " precisely correct the initial system alignment parameters to the currently\n";
 snotes[17] = 
 " viewed celestial sphere.\n";
 snotes[18] =
 "                              CONVENTIONS                                 \n";
 snotes[19] = 
 " AZIMUTH:  South = 0 deg, West = 90 deg, North = 180 deg, East = 270 deg. \n";
 snotes[20] = 
 "            ALTITUDE :  Horizon = 0 deg,  Zenith = 90 deg.                 \n ";
 snotes[21] =
 "                                                                  JE 4/97";

 wnotes = w_open(1,1,78,23);
 w_umessage(wnotes, " STARTRAC  -  Notes to User ");
 w_lmessage(wnotes, " press any key to continue");
 pen_color = d_colorattr(WHITE,BLUE,DISABLE,DISABLE);
 w_pencolor(wnotes,pen_color);

 w_poscurs(wnotes,0,1);
 for (i=1; i<22; i++) w_print(wnotes, snotes[i]);

 k_getkey();   /* wait for key press */
 w_close(wnotes);
 return;

}
