/********************************************************************/
/*				M_RDTPOS.C			    */
/*------------------------------------------------------------------*/
/*	TASK: 	Read Telescope Position	  			    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	7-1-97 					    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*	Reads telescope position and updates globals tazmd, taltd,  */
/*	also azmd, altd, and via s_horteq() call, phar & decr.	    */
/*	First reads ALT '2X1' then AZM '1X1'			    */
/*      Passed to: ccflg, 0 don't (normal), 1 correct syst coords   */
/*      --Syst Coord Corr (ccflg=1) not used in this compilation--  */ 
/********************************************************************/

#include <stdlib.h>
#include <string.h>
#include "stflib.h"
#include "window.h"

extern char instr[];	
extern double tazmd, taltd, azmd, altd, ppdazm, ppdalt;
double azmcd, altcd;	/* defined here, not curr used - save for later  */

void m_rdtpos(int ccflg)
{
 int i;  
 long chazm, chalt;
 double chazp, chalp;
 char *sptr, rplystr[10];

 
 /** tc_smode(5);  instead stop motors(11) placed in all move routines */


 for(i=0; i<10; i++) rplystr[i] = ' ';  
 tc_smode(10);	/* '2X1' to read alt change */
  sptr = strchr(instr, 'X');   /*looking of 'X' in reply 1X1<>+nnnnnnnn */
  for(i=0; i<9; i++) rplystr[i] = *(sptr+3+i);
  rplystr[9] = '\0';
  chalt = atol(rplystr);  

 for(i=0; i<10; i++) rplystr[i] = ' '; 	
 tc_smode(9);	/* '1X1" to read azm change */
  sptr = strchr(instr, 'X');
  for(i=0; i<9; i++) rplystr[i] = *(sptr+3+i);
  rplystr[9] = '\0';
  chazm = atol(rplystr);

 chazp = chazm;  chalp = chalt;

 tazmd += chazp/ppdazm;
 taltd += chalp/ppdalt; 

 if(tazmd < 0.0 ) tazmd += 360.0;	/* make + angle */
 if(tazmd >= 360.0) tazmd -= 360.0;     /* make between 0.0 and 359.9 */

 /***if(ccflg==1)  correct coord system by calculating azmcd, altcd
   { azmcd = tazmd - azmd;  altcd = taltd - altd; }   * not used here **/

 azmd = tazmd;
 altd = taltd;

 return;
} 

