/********************************************************************/
/*				MW_SPCLM.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Special Controls Menu 				    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	8-1-97	 				    */
/*	LAST UPDATE:	9-5-97  				    */
/*------------------------------------------------------------------*/
/*	Calls StarTrac Special Control Routines			    */
/********************************************************************/

#include <stdlib.h>
#include "window.h"
#include "menu.h"
#include "defaults.h"
#include "stflib.h"


mw_spclm()
{
 int i, menu_choice;
 Menu mwspc;
 Menu_Item item[5];
 char *SCtitle =       " SPECIAL CONTROLS MENU ";
 char *SCnames[5] =  { "    ~Filter Selection    ",
                       "  ~Camera Focus Control  ",
                       "    Field ~De-Rotation   ",
                       "  Data ~Logging Control  ",
                       " Return to ~Opening Menu " };

 do
  {
   mwspc = m_create(SCtitle, 28, 13, 24, 5);
   for(i=0; i<5; i++) 
    {
     item[i] = m_additem(mwspc,SCnames[i]);     
    }
    m_open(mwspc);
    menu_choice = m_lastindex(mwspc);
    switch(menu_choice)
     {
      case 0:  { m_free(mwspc);    break; }
      case 1:  { m_free(mwspc);    break; }
      case 2:  { m_free(mwspc);    break; }
      case 3:  { m_free(mwspc);    d_logger(0); break; }
      case 4:  { m_free(mwspc);    break; }
     }
   } while(menu_choice != 4);
 return;
}
