/********************************************************************/
/*				TC_STRNG.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Telescope Command Strings			    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-8-97  				    */
/*	LAST UPDATE:	10-18-97 				    */
/*------------------------------------------------------------------*/
/*	Makes Up Strings and Calls tmc_gnio(), then returns	    */
/*	Special Note: Normal Move has test for ZERO AZM problem     */
/********************************************************************/

#include <stdio.h>
#include <string.h>
#include "stflib.h"

extern char ostr[60];
extern int zazflg;

void tc_smode (int mode)	/* set up modes */
{
 char  cmini[] = " E SSA0 SSC1 SSF0 X0 J0";
   /*compumtr-2100 init; enable RS232, pulse mode,'L',V hi,A1,clr,joy disab */ 
 char  mncsi[] = "SSF0 X0";	/* normal moves set up */
 char  mccsi[] = "SSF1 X0";		/* continuous moves set up */
 char  mjoyi[] = "X0 J1";          /* joystick intialize & turn "on" */
 char  mjoyk[] = "J0";			/* joystick kill "off" */
 char  mstop[] = "S";			/* motor's stop */
 char  msngo[] = "G";			/* general GO */
 char  mjoys[] = "1SJ";			/* request joystick status */
 char  mpsx1[] = "1X1";			/* get azm posn */
 char  mpsx2[] = "2X1";			/* get alt posn */
 char  mpsx3[] = "3X1";			/* get field rot angle */
 char  mstpr[] = "S 1CR";		/* stop w/reportback AZ */
 char  suend[] = { 0x0D, '\0' };	/* set up end = CRtn, EOStr */
  
 switch(mode)
  {
   case 0: { strcpy(ostr,cmini); strcat(ostr,suend); break; } /* init cpumtr */
   case 1: { strcpy(ostr,mncsi); strcat(ostr,suend); break; } /* mn setup */
   case 2: { strcpy(ostr,mccsi); strcat(ostr,suend); break; } /* mc setup */
   case 3: { strcpy(ostr,mjoyi); strcat(ostr,suend); break; } /* joy set up */
   case 4: { strcpy(ostr,mjoyk); strcat(ostr,suend); break; } /* joy kill */
   case 5: { strcpy(ostr,mstop); strcat(ostr,suend); break; } /* gen stop */
   case 6: { strcpy(ostr,msngo); strcat(ostr,suend); break; } /* gen GO */ 
   case 7: break;
   case 8: { strcpy(ostr,mjoys); strcat(ostr,suend); break; } /* joy stat */
   case 9: { strcpy(ostr,mpsx1); strcat(ostr,suend); break; } /* az pos */
   case 10:{ strcpy(ostr,mpsx2); strcat(ostr,suend); break; } /* alt pos */
   case 11:{ strcpy(ostr,mstpr); strcat(ostr,suend); break; } /* stop/repbk */
  }
 if(mode >= 0 && mode < 8)  tmc_gnio(1);  /* send command & wait for 1st CR */
 else if(mode >= 8 && mode < 12) tmc_gnio(2);  /* send cmd, wait 2nd CR */
 return;
}


void tc_moven (long aznum, long altnum)		/* MOVE NORMAL */
{
 long  azabsv;
 char  azmcbuf[10], altcbuf[10]; 
 char  mncse[] = { ' ','I', ' ', 'G', ' ','1', 'C','R',
                              ' ','2','C','R', 0x0D, '\0' };
 char  mncss[] = "MN 1A1 1V10 2A1 2V6 1D";
    /*AZM V changed 10/18 from16to10, also fix tsmvazm to <= 16 in m_mra */
    /* ? stall out of AZM motors when cold and hi-V ? */
 char  mncsc[] = " 2D";

 /*** This is the FIX for the ZERO AZM Problem--send min +or-4 to az mot **/
 if(aznum < 0) azabsv = -aznum;		/*convert to + number*/
 else azabsv = aznum;
 if(azabsv < 4 && zazflg == 0)  { aznum = 4; zazflg = -1; }
 else if(azabsv < 4 && zazflg == -1) { aznum = -4; zazflg = 0; }
 
 sprintf(azmcbuf, "%-1ld", aznum);	/* max,min = +-99,999,999 */
 sprintf(altcbuf, "%-1ld", altnum);
 
 strcpy(ostr,mncss);
 strcat(ostr,azmcbuf); 
 strcat(ostr,mncsc);
 strcat(ostr,altcbuf);
 strcat(ostr,mncse);
 tmc_gnio(3);        /* send command, wait for 3rd CR */
 return;
}

	
void tc_movec(int type, float azvel, float altvel)   /* MOVE CONTINUOUS */
{
 union mc_info cmdst;
 char  azmcbuf[8], altcbuf[8];
 char  mccen[] = { ' ', 'I', ' ', 'G', 0x0D, '\0' };   /* end of string w/G */
 char  mccsp[] = " ";
 char  mccs1[] = "1S ";
 char  mccs2[] = "2S ";
 char  mcd1p[] = "MC 1A1 1H- 1V";
 char  mcd1n[] = "MC 1A1 1H+ 1V";
 char  mcd2p[] = " 2A1 2H- 2V";
 char  mcd2n[] = " 2A1 2H+ 2V";
 char  mcs1c[] = "MC 1V";
 char  mcs2c[] = " 2V";

 
 sprintf(azmcbuf, "%6.4f", azvel);	/* min,max = 0.0001 to 2.0000 */
 sprintf(altcbuf, "%6.4f", altvel);     /* sign (dir) by H+- */

 cmdst.all = type;
 
 strcpy(ostr,mccsp);
 if(cmdst.part.ax1_chng == 1) strcat(ostr, mccs1);
 if(cmdst.part.ax2_chng == 1) strcat(ostr, mccs2);

 if(cmdst.part.ax1_chng == 1)
   { if(cmdst.part.ax1_cdir == 0) strcat(ostr, mcd1p);
     else strcat(ostr, mcd1n);
   }
 else  strcat(ostr, mcs1c);
 strcat(ostr, azmcbuf);
    
 if(cmdst.part.ax2_chng == 1)
   { if(cmdst.part.ax2_cdir == 0) strcat(ostr, mcd2p);
     else strcat(ostr, mcd2n);
   }
 else strcat(ostr, mcs2c);
 strcat(ostr, altcbuf);

 strcat(ostr, mccen);	/* end of string */

 tmc_gnio(1); 	/* send command, wait for 1 CR */
 return; 

} 



                                                                                                                                                