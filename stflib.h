/********************************************************************/
/*				STFLIB.H			    */
/*------------------------------------------------------------------*/
/*	TASK:	StarTrac Function Library Header File		    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-20-97 				    */
/*	LAST UPDATE:	10-26-97  				    */
/*------------------------------------------------------------------*/
/*	Header file for StarTrac General Functions, unions, struct  */
/*	Contains ANSI C Type Declarations; arg and return lists     */
/*	by TYPE.						    */
/********************************************************************/

int   st_ptscr(void);	/* pix to screen */
void  s_snotes(void);	/* notes to user */
void  s_gconst(void);	/* compute/define general global constants */
void  s_horteq(void);   /* hor to equ calcuations */
void  s_sgdisp(void);   /* steer guide displays controller */

void  mw_openg(void);	/* opening menu Window version*/
void  mw_openm(void);   /* opening menu TRUE MENU version */
void  mw_movtr(void);   /* move track menu Window version */
void  mw_movtm(void);   /* move track menu TRUE MENU version */
void  mw_vernr(void);	/* modify vernier rates menu */
void  mw_spclm(void);	/* special function menu */

void  t_settim(void);	/* set time */
void  t_setdat(void);	/* set date */
void  t_osectc(long keycount, long eventcount );/* one sec time clock look */
void  t_sidtim(void);    /* sidereal and misc time/zone calculator */
void  t_cadisp(int mode); /* compute/update display parameters */
	         /* mode = initialize, normal, position, -----pos */

void  d_logger(int dlfcn); /* data logger control & write fcn */

signed long  dt_gtoj(char indate[]); 	/* gregorian to julian */
 
int   tmc_tsio(void);  /* telescope motor control - test I/O ckts */
void  tmc_gnio(int type);  /* telescope generic I/O routine */
void  tmc_inms(void);  /* initialize motor state */

void  tc_smode(int mode); /* telescope command strngs, to various modes */
void  tc_moven(long azm, long alt); /* move norm cmd string */
void  tc_movec(int dir, float azmvel, float altvel); /* mov cont, dir, vel */

int wn_gmseg(int msgn); /* general messg&cntrl windows, msgn = message # */


int m_mradec(int mode); /* move to RA/DEC, mode 0 = track, mode 3 = guide */
void m_mazalt(void);	/* move to AZ/ALT & hold */
void m_mjoyst(void);	/* move via Joystick */
void m_mhompk(void);    /* move to Home-Park */ 
void m_rdtpos(int ccflg); /* read Telescope position & update Pos'n param's */
int m_trackf(void);	/* tracking function */
int wn_trkms(int mode);	/* tracking fcn window mesgs & kb control */
			/* mode 0=RA/DEC, 1=ALT/AZ, 2=JOYST */


/*** Unions & Structures ***/

union mc_info		/* union for move continuous operations */
 {
  unsigned all;		/* whole, unsigned integer */
  struct		/* bit field structure: ax1 = AZM, ax2 = ALT */
   {
    unsigned empty_bt :1;	/* empty bit */
    unsigned ax1_chng :1;	/* direction change, 0=same, 1=change */
    unsigned ax2_chng :1;       /* as above */
    unsigned ax1_cdir :1;	/* current dir, 0=+ angle, H-; 1=-angle, H+ */
    unsigned ax2_cdir :1;        /* as above, axis 2 */
    unsigned ax1_pdir :1;	/* previous direction, as above */
    unsigned ax2_pdir :1;	/* as above, axis 2 */
    unsigned init_con :1;	/* initial condition = 0 */
    unsigned mc_spare :8;	/* spares */
   } part;
  };
