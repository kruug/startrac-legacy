/********************************************************************/
/*				MW_OPENM.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Startrak Opening Menu (true menu version)	    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-24-97 				    */
/*	LAST UPDATE:	10-2-97  				    */
/*------------------------------------------------------------------*/
/*								    */
/********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "window.h"
#include "menu.h"
#include "defaults.h"
#include "stflib.h"

extern int dsmode, dlfs;

mw_openm()
{
 int i, menu_choice;
 Menu mwopn;
 Menu_Item item[9];
 char *OPtitle =       " STARTRAC OPENING MENU ";
 char *OPnames[9] =  { "    Set System ~TIME     ",
                       "    Set System ~DATE     ",
                       "  ~RESET  2100 Indexer   ",
                       " ~Zero Coord.Corrections ",
                       "  Manual ~Command Mode   ",
                       " ~Special Controls Menu  ",
                       "  Go To ~MOVE/TRK Menu   ",
                       "  ~Key Operators Menu    ",
                       "       E~XIT to DOS      " };

dsmode = 1;	/* only time update for now, NLused */

k_idle(t_osectc);  /* if no key, update time/disp & retn */
	/* note: only has to be done once in pgm */

 do
  {
   mwopn = m_create(OPtitle, 28, 13, 24, 9);
   for(i=0; i<9; i++) 
    {
     item[i] = m_additem(mwopn, OPnames[i]);     
    }
    m_open(mwopn);
    menu_choice = m_lastindex(mwopn);
    switch(menu_choice)
     {
      case 0:  { m_free(mwopn);  t_settim();  break; }
      case 1:  { m_free(mwopn);  t_setdat();  break; }
      case 2:  { m_free(mwopn);  wn_gmesg(5); break; }
      case 3:  { m_free(mwopn);  wn_gmesg(2); break; }
      case 4:  { m_free(mwopn);  /*  not yet defined */ break; }
      case 5:  { m_free(mwopn);  mw_spclm();  break; }
      case 6:  { m_free(mwopn);  mw_movtm();  break; }
      case 7:  { m_free(mwopn);  /* not yet defined */ break; }
      case 8:  { m_free(mwopn);  break; }
     }
   } while(menu_choice != 8);
 
 fcloseall();
 w_closeall();
 exit(0);	/* terminate startrak */
}
