/********************************************************************/
/*				T_SIDTIM.C			    */
/*------------------------------------------------------------------*/
/*      TASK:	Sidereal Time Calculator                            */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	4-6-97	 				    */
/*	LAST UPDATE:	10-15-97 				    */
/*------------------------------------------------------------------*/
/*      This routine called ONCE at power TURN ON, OR if Date or    */
/*      Time are changed from the Opening Menu.                     */
/*	Passed to : nothing, gets and saves begin time as bestime   */
/*	Calls dt_gtoj() for julian from 1/1/1			    */
/*	Places 0 < Sideral Time (dp) < 24 hrs in global csidtm      */
/*      Also develops sidereal hr:min:sec and puts them in globals  */
/*      Equation for depx taken from CTRLN16.BAS w/my corr.         */
/*      At end calls t_cadisp(0) to produce telescope parm display  */
/********************************************************************/

#include <stdio.h>
#include <time.h>
#include "stflib.h"

/* globals */
int tzone;	      /* time zone of BC Observ, defined & set here */
extern double longd;  /* longitude of BC Observ */
double csidtm;        /* current sid time 24 hr basis, definition */
time_t bestime, lestime;  /* beginning time, last time, definition */
int sidhr, sidmin;	/* define */
double sidsec;          /* define */

t_sidtim(void)
{
 int  sidint, cmonth;
 signed long int julian;	/* has to be signed here if # is - ! */
 double sidtd, sidtfd, fGrTm, fDfGr, depx, sidfrc;
 char datestr[12];
 struct tm *cdtmptr;	

 time(&bestime);		/* beginning time */
 cdtmptr = localtime(&bestime);  /* beg tim to struct */
 cmonth = cdtmptr->tm_mon +1;	 /* current month */
 if(cmonth > 3 && cmonth < 11) tzone = 5;  /* CDT, April thru Oct */
 else tzone = 6;                           /* CST */
 sprintf(datestr,"%2d/%2d/%2d",
        cdtmptr->tm_mon+1,cdtmptr->tm_mday,cdtmptr->tm_year);
 julian = dt_gtoj(datestr) + 1721425;  /* my julian corrected */
 depx = (julian -2451545.5)/36525.0;	/* 2451545.5 == epoch 2000 */
 sidtd = (24110.54841 + (8640184.812866 +
                (.093104 - .0000062 * depx) * depx) * depx)/86400.0;
 fGrTm = (cdtmptr->tm_hour + tzone  + cdtmptr->tm_min/60.0 
    + cdtmptr->tm_sec/3600.0)/24.0;  /* Greenwich Time in frac of a day */
 fDfGr = longd/360.0;	/* Beaver Creek Obs from Greenwich in frac of a day*/
 sidtfd = sidtd + fGrTm * 1.0027379093 - fDfGr; 
       /* local sidereal time,fraction of a day,1 UTsec = 1.002738..SIDsec */
 sidint = sidtfd; 	/* integer portion of sidtfd*/
 sidtfd -=sidint;
 if(sidtfd < 0) sidtfd = sidtfd + 1.0;   /* make positive # */
 csidtm = sidtfd * 24.0;   /* put in global var location, 24 hr & frac basis */
  /* reduce to hr/min/sec */
 sidfrc = csidtm ;	/* sidfrc, hrs & fraction */ 
 sidhr = sidfrc;		/* integer portion, to global */
 sidfrc = (sidfrc - sidhr) * 60.0;	  /* make frc part into min & frc */
 sidmin = sidfrc;	       /*integer portion, to global */
 sidsec = (sidfrc - sidmin) * 60.0;  /*make frc part into sec(dp),to global */
 lestime = bestime;
 t_cadisp(0);	/* call and intialize tel parm display */
 return;
}
