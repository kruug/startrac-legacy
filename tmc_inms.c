/********************************************************************/
/*				TMC_INMS.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Initilaize CompuMotor State			    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-4-97  				    */
/*	LAST UPDATE:	6-4-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: nothing.  Returns: nothing			    */
/*      Currently; sets up pulse mode, load & go 'on', veloc normal */
/********************************************************************/

#include <string.h>
#include "stflib.h"

/* globals */

extern char ostr[40];

void tmc_inms(void)
{

 char sustr[]= {'S','S','A','0',' ','S','S','C','1',
                                  ' ','S','S','F','0', 0x0D, '\0'};
            /* pulse mode, load & go "on", normal velocity, CRtn, end str */
 strcpy(ostr, sustr);
 tmc_gnio(1);	/* send string &  wait for first CR */
 return;  
}





                                                