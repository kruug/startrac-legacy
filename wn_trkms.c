/********************************************************************/
/*			    WN_TRKMS.C  			    */
/*------------------------------------------------------------------*/
/*   	TASK: Windows for Startrac Tracking Control   	            */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-9-97				 	    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: Mode. Mode 0 is RA/DEC, 1 is ALT/AZ, 2 is JOY    */
/*      Mode 0 is the only place where SystCord can be corrected    */
/*      Returns: 0 = normal, -1 = excessive trking rates, -2 if     */
/*      below horizon, -3  Re-Initialize Coord System (del for now) */
/********************************************************************/

#include "window.h"
#include "keyboard.h"
#include "defaults.h"
#include "stflib.h"

extern int trkflg, genflg, vazmct, valtct, vernflg;

int wn_trkms(int mode)
{

 Window wtmsg;
 int key, rv, pen_color1, pen_color2;

 char *vstrng[4] =  { " ONE", " TEN", "  40", " 100" };

 pen_color1 = d_colorattr(RED,BLUE,ENABLE,DISABLE);
 pen_color2 = d_colorattr(WHITE,BLUE,DISABLE,DISABLE);
 w_pencolor(wtmsg, pen_color1);

 /* message & kb contol for tracking */
 wtmsg = w_open(10,20,60,4);
 w_umessage(wtmsg, " SELECTED OBJECT BEING TRACKED ");
 w_lmessage(wtmsg, " Keyboard Selectable Functions Listed Above ");
 w_printf(wtmsg, " To STOP Tracking Function & Return to Menu-press ");
 w_pencolor(wtmsg, pen_color2);
 w_printf(wtmsg,"Space Bar\n");
 w_pencolor(wtmsg, pen_color1);
 w_printf(wtmsg, " VERNIER Moves (+- %s Pixels) press & release ", vstrng[vernflg]);
 w_pencolor(wtmsg, pen_color2);    
 w_printf(wtmsg, "Arrow Keys\n");
 w_pencolor(wtmsg, pen_color1);
 /*** if(mode == 0) {
 w_printf(wtmsg, " Stop Tracking  &  RE-INITIALIZE Coord.System  -  press ");
 w_pencolor(wtmsg, pen_color2);
 w_printf(wtmsg, "F4\n");
 w_pencolor(wtmsg, pen_color1);
               }   deleted for now, hold for later use ***/
 w_printf(wtmsg, " Screen to VIEW/MODIFY Vernier Increment VALUES - press ");
 w_pencolor(wtmsg, pen_color2);
 w_printf(wtmsg, "F8 ");
 w_pencolor(wtmsg, pen_color1);   
     
   /** the following tests for key presses or trkflg reset = 0, 
            otherwise allows t_osectc to update trking loop **/
 do
  {
   while (!k_ready() && trkflg == 1); /* keep doing this until no key or trk */
  
   if(trkflg==0 && genflg ==1)	/* exited by excessive tracking rates */
    {
     w_close(wtmsg);
     return -1;		/* caller puts up "excessive rates " */
    }
   else if(trkflg==0 && genflg == 0)	/* exited by "below horizon " */
    {
     w_close(wtmsg);
     return -2;
    }  
   else			/* key must be ready */
    {
     key = k_getkey();
     if(key==_UPARROW) valtct++;
     if(key==_DNARROW) valtct--;
     if(key==_LARROW) vazmct--;
     if(key==_RARROW) vazmct++;
     
    /****if(mode==0 && key==_F4) 
      { w_close(wtmsg);  return -3;} DELETED FOR NOW Re-init Coord System **/ 
    
     if(key==_F8)
      {
       mw_vernr();	/* put up vern rate menu */
       w_poscurs(wtmsg, 19, 1);
       w_printf(wtmsg, "%s", vstrng[vernflg]);  /* update screen */
      }	
    }
  } while (key != ' ');		/* space key causes routine exit */   

 w_close(wtmsg);
 return 0;	/* returns to initial caller == move  routine */

}
