/********************************************************************/
/*				T_OSECTC.C			    */
/*------------------------------------------------------------------*/
/*	TASK: One Second (look at) Time Clock  (TWO SEC TRK/DISP)   */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	4-30-97 				    */
/*	LAST UPDATE:	8-24-97 				    */
/*------------------------------------------------------------------*/
/*	Looks at DOS Clock (hundreths of sec), and if > 200 msec is */
/*	left returns to caller. If <= 200 msec left waits until new */
/*	second arrives and then goes to "one second critical" fcns. */
/*      On alternate seconds the track & go/display fcns are done.  */
/*      The Go/Display fcn is perf on even seconds, the Track loop  */
/*      calculations are executed on odd seconds.                   */
/*      Also manages the 100 msec flag	 			    */
/*      ** Use this & accompanying routines for SLOW Computers**    */
/********************************************************************/

#include <dos.h>

/* global */ 
extern int trkflg;
int dsmode;	/* definition, display update mode for t_cadisp(dsmode) */
int hmsflg;     /* definition, hundred msec flag, set here, user resets */

void t_osectc(long keycount, long eventcount)  /*var passed dueto k_idle */
{
 int  csec, nsec, bsec, rv, currhsec, diffhsec;
 static updsflg, lasthsec;
 struct time dostmbuf;
 
 dsmode = 1;	/****temporary, remove after other routines done! ***/ 

 gettime(&dostmbuf);	/* get current dos time */
 currhsec = dostmbuf.ti_hund;	/* current hundreth of sec */
 diffhsec = currhsec - lasthsec;

 /* hmsflg manager */
 if(diffhsec < 0) lasthsec = currhsec;
 if(diffhsec >= 10)
  { hmsflg = 1;  lasthsec = currhsec; }	/* set hundred msec flag */

 /* critical one sec time test */  
 if( currhsec < 80) return; 	/* if > 200 msec left */
 else
  {
   csec = dostmbuf.ti_sec;
   nsec = (csec +1)%60;
   while(csec != nsec)	/* loop until new second arrives */
    {
     gettime(&dostmbuf);
     csec=dostmbuf.ti_sec;
    }

   bsec = csec % 2;	/* even/odd sec? */

   /* call time dependant rotines */
   if(bsec == 0)	/* even sec */
    {
     t_cadisp(dsmode);		/* update displays */
    }
   if(bsec ==1)		/* odd sec */
    {
     if(trkflg == 1)  rv = m_trackf();   /* do next pass of tracking loop */
     else t_cadisp(dsmode);	/* if no tracking, update disp every sec */
    }
   return;	/* return to original caller */
  }
}


                                                