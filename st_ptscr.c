/**  ST_PTSCR.C   PICTURE for STARTRAK; routine dev 5/20/94, mod 6/23/96 **/

#include <bios.h>
#include <stdio.h>
#include <dos.h>
#define COLORCARD 0xB8000000;  /** colorcard address - start of page 0 **/


int st_ptscr(void)
{ 
 FILE *fp;
 int i;
 char s;
 char far *p;
 
 p = COLORCARD;		/* sets pointer p to videoscrn, page 0 */
 clrscrn();
 setvpage(1); 
 clrscrn();
 fp = fopen("STARTRAC.FGB", "rb");
 if(fp != NULL)
  { 
   for(i=0; i<4000; i++)
    {
     fread(&s, sizeof(char),1,fp);
     *p++ = s;
    }
   fclose(fp);
   cursoff();
   setvpage(0);  /* set to page 1 */
   sleep(3);	/* wait 3 seconds */ 
   curson();
   clrscrn();
   return 0;
  }
 else 
  { 
   setvpage(0); 
   printf("CANT OPEN FILE STARTRAC.FGB\n"); 
   return -1;
  }
}

