/********************************************************************/
/*				TMC_GNIO.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Telescope Motor/Computer Generic I/O Routine	    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-28-97 				    */
/*	LAST UPDATE:	7-27-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: type; 0=no wait, 1=wait 1CR, 2=wait 2nd CR,      */
/*      3=wait 3 CRtns. SubRtn returns: none                        */ 
/********************************************************************/

#include <bios.h>
#include "window.h"

/* globals */
char ostr[60];		/* define, output string to mtrs */
char instr[60];		/* define, responses from mtrs */

void  tmc_gnio(int type)
{ 

 int i, j, k, compn, stat0, stat1, stat2, stat3, flush;

 compn = 1; /*COM2*/

 flush = bioscom(2,0,compn);	/* flush rx buff */

 i = 0; j = 0; k = 0;

 switch(type)
  {
   case 0:  /*IMPORTANT, DO NOT REMOVE, these tests needed for FAST LOOP */
   do
    { 
     stat3 = bioscom(3,0,compn);	/* read status word */
     if((stat3 & 0x2000) == 0x2000 && (ostr[i] != '\0'))  /* TX rdy + data */      
     stat1 = bioscom(1,ostr[i++],compn);	/* sends out char */
     if((stat3 & 0x0100) == 0x0100)	/* rx rdy */ 
      {   
       stat2 = bioscom(2,0,compn);
       if((stat2 & 0x00FF)==0x000D) k++;	/* if CRTn */
       instr[j++] = stat2 & 0x00FF;
      }   
    } while( ostr[i] !='\0');    /* typ0 & tx !finished */
    return;
    break; 
  
   case 1:
   do
    { 
     stat3 = bioscom(3,0,compn);	/* read status word */
     if((stat3 & 0x2000) == 0x2000 && (ostr[i] != '\0'))  /* TX rdy + data */      
      stat1 = bioscom(1,ostr[i++],compn);	/* sends out char */
     if((stat3 & 0x0100) == 0x0100)	/* rx rdy */ 
      {   
       stat2 = bioscom(2,0,compn);
       if((stat2 & 0x00FF)==0x000D) k++;	/* if CRTn */
       instr[j++] = stat2 & 0x00FF ;
      }   
    } while(k < 1);     /* typ1 & !first CRtn */
   return;
   break;

 case 2:
   do
    { 
     stat3 = bioscom(3,0,compn);	/* read status word */
     if((stat3 & 0x2000) == 0x2000 && (ostr[i] != '\0'))  /* TX rdy + data */      
       stat1 = bioscom(1,ostr[i++],compn);	/* sends out char */
     if((stat3 & 0x0100) == 0x0100)	/* rx rdy */ 
      {   
       stat2 = bioscom(2,0,compn);
       if((stat2 & 0x00FF)==0x000D) k++;	/* if CRTn */
       instr[j++] = stat2 & 0x00FF;
      }   
     /* if(k_ready()) k_getkey(); dummy to clear out any wrong keypress */
     /* } while(k < 1 || (!k_ready() && k < 2));   typ2 & !2nd CR,   */
     /* these were in old version, they don't work!  */
    } while(k < 2);
   return;
   break;

 case 3:
   do
    { 
     stat3 = bioscom(3,0,compn);	/* read status word */
     if((stat3 & 0x2000) == 0x2000 && (ostr[i] != '\0'))  /* TX rdy + data */      
       stat1 = bioscom(1,ostr[i++],compn);	/* sends out char */
     if((stat3 & 0x0100) == 0x0100)	/* rx rdy */ 
      {   
       stat2 = bioscom(2,0,compn);
       if((stat2 & 0x00FF)==0x000D) k++;	/* if CRTn */
       instr[j++] = stat2 & 0x00FF;
      }   
    } while(k < 3);    /* typ3 & !3rd CR  */
   return;
   break;
  
  }	/* switch end */
}
