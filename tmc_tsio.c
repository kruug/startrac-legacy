/********************************************************************/
/*				TMC_TSIO.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Telescope Motor/Computer Enable & Test I/O	    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-24-97 				    */
/*	LAST UPDATE:	10-2-97   Re_written to incl GUIDE port	    */
/*------------------------------------------------------------------*/
/*	Passed to: nothing.  Return responses: -1 is a failure,     */
/*      exits @ main, 0 is success everything OK. CompMtr is COM2   */
/*      (1) set up UART, (2) attempt to send 'E' & 'CR', then first */
/*      check for DSR & DCD (connection OK?), followed by response  */
/*      of 'E' & 'CR'.  Note: also COM1 setup for GUIDE             */
/********************************************************************/

#include <bios.h>
#include <dos.h>
#include "window.h"


int  tmc_tsio(void)
{ 

 int i, j, k, m, compn, stat0, stat1, stat2, stat3, resp, try;
 char ostr[3];
 int  istr[6];
 Window wtst;


 bioscom(0, 0xE7, 0); /* GUIDE port COM1, 9600baud, no p, 2 stp, 8 data */

 /** CompuMotor set up @ test **/
 compn = 1;	/* COM port #, 0 = COM1, 1 = COM2 */
 stat0 = bioscom(0, 0xE0|0x00|0x04|0x03, compn); /* set up port to motors */  
              /* 9600 Baud, no Parity, 2 Stop bits, 8 Data bits */
 j=0; for(i=0;i<64;i++) j++;	/* SIMPLE DELAY for slow 8251 hardware */

  /* Now try to send 'E' followed by 'CR' */ 

  try = 0;
  while( try < 2)
   {
    /* try to enable compumotor RS 232 */    
    try++;
    ostr[0] = 'E'; ostr[1] = 0x0D;   /* E + CRTN , enable for compumtrs */
    for(i=0; i<6; i++) istr[i] = 0;
    k = 0; j = 0; m = 0;
    do
     {
      m++;	/* timeout ctr */
      stat3 = bioscom(3,0,compn);	/* read status word */
      if((stat3 & 0x0100) == 0x0100)	/* rx rdy */
       {
        stat2 = bioscom(2,0,compn);	/* read rx */
        istr[k++] = stat2;		/* save */
       }
      if((stat3 & 0x2000) == 0x2000 && j < 2)  /* TX rdy w/remaining data */      
        stat1 = bioscom(1,ostr[j++],compn);	/* sends out char */
     } while((stat2 & 0x00FF) != 0x000D && m < 512);
            /* != CRTN AND m not yet 512 == !Timedout */

     /** following test for DSR, & DCD bits **/  
     if((stat3 & 0x00A0)!=0x00A0)  /* no DSR, DCD == no connection */
      {
       wtst = w_open( 10,13,60,3); 
       w_poscurs(wtst,0,1);    	
       w_printf(wtst, "   Failed Connection Between Computer & Telescope Motors ");
       sleep(5);
       w_close(wtst);
       return(-1);		/* failed connection - exit */
      }
 
    /** if above OK, then next test is for 'E' followed by 'CR' **/  
    if((stat2 & 0x00FF)== 0x000D && (istr[k-2] & 0x00FF) == 0x0045) 
     {
      if(try==2) w_close(wtst);  
      return(0);	/* OK, rcvd 'E' back, sucessful exit  */
     }
    else if(try==2) { w_close(wtst); return(-1); }  /* failed exit*/

    else		/* not OK, try ==1, put up message Turn "On" TS*/
     {
      wtst = w_open(22,13,36,4);
      w_poscurs(wtst,0,1);
      w_printf(wtst, "    TURN TELESCOPE POWER ON NOW    \n");
      w_printf(wtst, "      press any key when done      ");   
      k_getkey();
     }
   }
  w_close(wtst);	/* failed exit */
  return(-1);
}


                                                       