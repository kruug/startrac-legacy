/********************************************************************/
/*				STARTRK2.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Startrak Main Fcn (TWO SEC VERSION)		    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	4-18-97 				    */
/*	LAST UPDATE:	7-19-97  				    */
/*------------------------------------------------------------------*/
/*	Sets up initial values & calls opening menu		    */
/********************************************************************/

#include<stdlib.h>

#include "window.h"
#include "defaults.h"

#include "stflib.h"

/* static globals */
   /* non for this routine */

main()
{

 int i, r, xx;
 Window  whdr;


 r = st_ptscr();	/* pix to scrn */

/* set default colors */
 d_colorset(WHITE, COLOR(RED+BRIGHT,BLUE), COLOR(YELLOW+BRIGHT,BLUE),
            COLOR(WHITE+BRIGHT,BLUE), COLOR(RED,WHITE),
            COLOR(WHITE+BRIGHT, YELLOW), COLOR(BLACK, BLACK));

 whdr = w_open (1,1,78,2);	/* header */
 w_poscurs(whdr,20,0);
 w_printf(whdr, "STARTRAC.EXE    VER 1.0     6/30/97 ");
 w_poscurs(whdr,21,1);
 w_printf(whdr, "XECON ASSOCIATES  EAU CLAIRE, WI.");

 r = tmc_tsio(); /* Test for I/O connection, Telescope Power On & Response */
 if(r==-1) { w_closeall(); exit(0); }

 tmc_inms();	/* initialize motor state */ 

 s_gconst();	/* define/compute gen global constants */
 s_snotes();    /* display notes */

 t_sidtim();	/* initialize sidereal, and via call, std time & parm disp */ 

 mw_openm();    /* opening menu */

}

