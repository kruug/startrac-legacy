/********************************************************************/
/*			    M_MAZALT.C  			    */
/*------------------------------------------------------------------*/
/*   	TASK: Move to Azimuth & Altitude Routine (Startrac)         */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	7-8-97			        	    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: nothing.  Returns 0 if OK	        	    */
/*      Console Inputs: Objects NAME, ALT & AZM                     */
/*           						            */
/********************************************************************/

#include <stdio.h>
#include <math.h>
#include "window.h"
#include "field.h"
#include <string.h>
#include <stdlib.h>
#include "stflib.h"

extern int trkflg, pcflg, vazmct, valtct;
extern int dlfs, sidhr, sidmin;
extern char dlgbuf[], ltimstr[];
extern double azmd, altd, phar, pdecr, sidsec;
extern double dtor, rtod, htod, coslat, sinlat;
extern double tazmd, taltd, ppdazm, ppdalt;

void m_mazalt(void)
{
 char iname[15], inazm[15], inalt[15];

 char *prompt[] =   { "   Object's Name/Description      ",
                      "   Object's Azimuth Angle         ",
                      "   Object's Altitude Angle        " };
  
 char *template[] = { "______________ ",
                      "____._______deg",
                      " __._______ deg" };

 int  range[] = { GRAPHIC,BLANK|SIGN|NUMERIC, BLANK|NUMERIC };

 Window   winput, wsmsg;
 Field fd[3];

 int i, j, rv, sisec, btkey;
 char tmpbuf[20], modstr[4];
 long lpmazm, lpmalt;
 double cpazmd, cpaltd;  /* new stuff */
 double dmvazmd, dmvaltd, ppmazm, ppmalt, coranr, paltcr, pazmcr; 

 sprintf(modstr, "AAH");

 winput = w_open(8, 12, 64, 5);	/* input window */
   /* w_umessage(winput, " OBJECT/COORDINATES - INPUT WINDOW "); */
   w_umessage(winput, " MOVE TO AZMIMUTH/ALTITUDE - INPUT COORDINATES ");
   w_poscurs(winput,0,0);
   w_printf(winput, "Ranges: Az (0 to +-359.99 deg, 0=South), Alt (0 to 89.99 deg) \n");
   for(i=0; i<3; i++)
    {
     fd[i] = f_create(prompt[i], template[i]);
     f_range(fd[i], range[i]);
     f_append(winput, 5, i+1, fd[i]);
    }
   w_poscurs(winput,0,4);
   w_printf(winput,"      ---press RETURN after all data has been entered---");
   f_return(fd[2], ENABLE);
   btkey = f_batch(winput);
   w_poscurs(winput,0,4);
   w_printf(winput,"                                                        ");
   f_getmasked(fd[0], iname);
   f_getmasked(fd[1], inazm);
   f_getmasked(fd[2], inalt);
 
   for(i=0; i<3; i++) f_free(fd[i]);	/* free up fields */

   if(btkey == 27 || btkey == -1)
     { w_close(winput); return -1; }	/* on ESCape key of ERR code -1 */

   /*****process Kb Inputs *********/
   cpazmd = atof(inazm);	/* azm */
   cpaltd = atof(inalt);        /* alt */

   /* set up */
   /****** trkflg = 0;  /* initial */

   /* tests */
   if(cpaltd >= 89.9997)  /* forbidden zone < 1.5 arc sec frm 'zero' */
    {
     rv = wn_gmesg(7);  /* forbidden altitude message */
     w_close(winput);
     return 0;		/* return to M/T Menu */
    }  
     
   if(cpaltd < 0.0)	/* object below horiz */
    {
     rv= wn_gmesg(1);	/* below horiz message & wait for key pr */
     w_close(winput);
     return 0;	/* return to menu */    
    }

   if(cpazmd < 0.0) azmd = cpazmd + 360.0;
   else azmd = cpazmd;		/* make azmd + angle */
   altd = cpaltd;

   /* platform corrections */
   coranr = (azmd - 60.0)*dtor;
   paltcr = -0.0020944 * sin(coranr); /* platform cor radians */
   pazmcr = -0.00200 * cos(coranr)/cos(altd*dtor);	/*pltfrm cor radians */
   altd = altd + paltcr*rtod; 		/* corrected value */
   azmd = azmd + pazmcr*rtod;		/* corrected azmd */
  
   s_horteq();


 
     /** additions for d_logger **/
      if(dlfs==1)
       {
        sprintf(tmpbuf, "START ");
        strcpy(dlgbuf, tmpbuf);
        strcat(dlgbuf, ltimstr);	/*local time */
        sisec = sidsec;  /* converts double to int */
        sprintf(tmpbuf, "; %s;ST%2d:%2d:%2d; ", modstr,sidhr, sidmin, sisec);
        strcat(dlgbuf,tmpbuf);        
        strset(tmpbuf, '\0');
        strncpy(tmpbuf, iname, 14);
        strcat(dlgbuf,tmpbuf);
        sprintf(tmpbuf,"; AZ%9.5f d; ", azmd);
        strcat(dlgbuf, tmpbuf);          
        sprintf(tmpbuf,"AL %9.5f d;\n", altd );
        strcat(dlgbuf, tmpbuf); 
       }
     /** end of first d_logger segment **/

   
   /* ok to proceed , put up slewing message */
   wsmsg = w_open(23,20,34,2);
   w_printf(wsmsg, "             --WAIT--           \n");
   w_printf(wsmsg, " SLEWING TELESCOPE INTO POSITION ");        
      /* close this window just before calling tracking */

       
   /*** now calc move distance ***/
   dmvazmd = (azmd - tazmd); 
   if(dmvazmd >  180.0) dmvazmd -=360.0;   /* get move between +- 180 deg */
   if(dmvazmd < -180.0) dmvazmd +=360.0;
   dmvaltd = (altd - taltd);

   ppmazm = dmvazmd * ppdazm;   	/* pulses per move , pulses per deg */  
   ppmalt = dmvaltd * ppdalt;

                 /* do basic move here */
   tc_smode(1);  /* set to move Norm; A1, 1V16, 2V6 */
   lpmazm = ppmazm;  lpmalt = ppmalt;
   tc_moven(lpmazm, lpmalt);	/* move telescope *      

     /* later put test here for move verification */  

    tazmd += dmvazmd; 
    if(tazmd < 0.0) tazmd +=360.0;
    if(tazmd >=360.0) tazmd -=360.0; 
    taltd += dmvaltd;  /* t position after move */

       /* now holding for key press to strt trackng */
    
    w_close(wsmsg);
    rv = wn_gmesg(4);   /* message, press key to start trk or exit */     
    if(rv == -1)	/* Escape */
     {
      w_close(winput);
      return;
     }
    else		/* begin tracking */
     {
                        /* set up */
      trkflg = 0;  pcflg = 1; vazmct = 0;   valtct = 0;
      rv = m_trackf();	 /* first pass */
      if(rv < 0)	 /* abort from initialize */
       {
        if(rv == -1)   wn_gmesg(3);	/* rates excessive */
        else if(rv == -2)   wn_gmesg(1); /* below horiz */        
        trkflg = 0;  	/* precaution */
        tc_smode(5);	/* stop motors precaution only */
        w_close(winput);
        return;
       }
      else	/* tracking in progress */
       {
        rv = wn_trkms(1);
          /* @ return stop & cleanup */
        trkflg = 0;
        tc_smode(11);	/* stop motors w/rptbk */
        if(rv == -1)  wn_gmesg(3);	/* excessive rates */
        else if(rv == -2) wn_gmesg(1);	/* below horiz */
        m_rdtpos(0);	/* update position */



        /*** final d_logger section **/
        if(dlfs == 1)
         {
          sisec = sidsec;
          sprintf(tmpbuf, "END:ST%2d:%2d:%2d; ", sidhr, sidmin, sisec);
          strcat(dlgbuf, tmpbuf);
          sprintf(tmpbuf, "AZ %9.5f d; ", azmd);
          strcat(dlgbuf, tmpbuf);
          sprintf(tmpbuf, "ALT %8.5f d; ", altd);
          strcat(dlgbuf, tmpbuf);
          sprintf(tmpbuf, "HA %9.5f d; ",  phar * rtod);
          strcat(dlgbuf, tmpbuf);
          sprintf(tmpbuf, "DE %9.5f d; \n\n", pdecr * rtod);
          strcat(dlgbuf, tmpbuf);
         }

        w_close(winput);
        if(dlfs==1) d_logger(1);   /* write to disk */
        return; 	/* to menu */
       }
     }
}
