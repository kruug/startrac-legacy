/********************************************************************/
/*				T_CADISP.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Compute & Display Telescope Parameters      	    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	4-30-95 				    */
/*	LAST UPDATE:	10-27-97  TEST VERSION			    */
/*------------------------------------------------------------------*/
/*      Passed to: mode; defines proc: 0 sets up initial window and */ 
/*      display, bit 1 "on" is normal entry, bit 2 "on" updates the */
/*      positions display, bit 3 "on" puts --- in positions display.*/    
/*	Gets current time (cestime), computes current sidereal time,*/
/*      and displays current date, time and sidereal time.	    */
/*	Saves cestime in lestime, for loop check at next pass	    */
/********************************************************************/

 #include <stdio.h>
 #include <time.h>
 #include "window.h"
 #include "defaults.h"
 #include "stflib.h"

 extern double azmd, altd, phar, pdecr;	
          /* defined elsewhere, display values for az,alt,ha,dec */
 extern time_t lestime;
 extern int sidhr, sidmin, tzone;
 extern double csidtm, sidsec, rtod;
 char ltimstr[12];  /* defined here as global for local time logger use */

 /* old remove extern double cazmd, caltd, crad, cdecd; */

 Window wcpar;		/* made global to allow re-entry */

 void t_cadisp(int mode)
 {

  time_t cestime;
  struct tm *cdtmptr;
  char stimstr[12], cdatstr[16];
  int i,pen_color, sidisec;
  double fsidsec, dtime, phafd, pdecfd; 

  if(mode==0)	   /* initial entry, set up window */
   {
    w_close(wcpar);	/* close if previously opened (for re-entry) */
    wcpar = w_open(1,6,78,3);
    w_umessage(wcpar, " CURRENT TELESCOPE PARAMETERS ");
    w_poscurs(wcpar,1,0);
    w_printf(wcpar,"                          Todays Date is                        \n");
    w_printf(wcpar," SIDEREAL TIME   --:--:--  HOUR ANGLE  ----.-------d   AZIMUTH ----.-------d\n");
    if(tzone==6)
    w_printf(wcpar," CENTRAL S TIME  --:--:--  DECLINATION  ---.-------d   ALTITUDE  --.-------d"); 
    else
    w_printf(wcpar," CENTRAL D TIME  --:--:--  DECLINATION  ---.-------d   ALTITUDE  --.-------d");   
   }

  pen_color = d_colorattr(WHITE,BLUE,DISABLE,DISABLE);
  w_pencolor(wcpar, pen_color);

  time(&cestime);	/* get current time */
  dtime = difftime(cestime, lestime);  /* time difference */
  cdtmptr = localtime(&cestime);
  sprintf(cdatstr, "%2d/%2d/%4d", cdtmptr->tm_mon+1, cdtmptr->tm_mday,
                                  cdtmptr->tm_year + 1900);
  w_putsat(wcpar, 41,0, cdatstr);
  sprintf(ltimstr, "%2d:%2d:%2d", cdtmptr->tm_hour, cdtmptr->tm_min,
                                  cdtmptr->tm_sec);
  w_putsat(wcpar, 17,2, ltimstr);

  fsidsec = 1.0027379093 * dtime;
  csidtm += fsidsec/3600.0;	/* updates csidtm */
  sidsec += fsidsec;	/* updates sid sec/min/hr */

  if(sidsec >= 60.0) 
   {
    while(sidsec >= 60.0)
     {
      sidsec -= 60.0;
      sidmin++;
     }
    if(sidmin >= 60)
     { 
      sidmin -= 60;
      sidhr++;
      if(sidhr >= 24) sidhr -= 24;
     }
   }
  sidisec = sidsec; /*integer version*/
  sprintf(stimstr, "%2d:%2d:%2d", sidhr, sidmin, sidisec);
  w_putsat(wcpar, 17,1,  stimstr);
  lestime = cestime;

  phafd = phar * rtod;
  pdecfd = pdecr * rtod;
  
  if(phafd >  180.0) phafd -= 360.0;	/* make between +-180.0 */
  if(phafd < -180.0) phafd += 360.0; 
 
  sprintf(cdatstr, "%11.6f d", phafd);
  w_putsat(wcpar, 39, 1, cdatstr);
  
  sprintf(cdatstr, "%11.6f d", azmd );
  w_putsat(wcpar, 63, 1, cdatstr);

  sprintf(cdatstr, "%10.6f d", pdecfd );
  w_putsat(wcpar, 40, 2, cdatstr);

  sprintf(cdatstr, "%9.6f d", altd );
  w_putsat(wcpar, 65, 2, cdatstr);   

  return;
 }    
 	

