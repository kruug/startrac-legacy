/********************************************************************/
/*				S_GCONST.C	(TWO SEC)	    */
/*------------------------------------------------------------------*/
/*	TASK:	StarTrac General Constants Define & Compute	    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-02-97 				    */
/*	LAST UPDATE:	11-23-97  				    */
/*------------------------------------------------------------------*/
/*	General Global Constants for StarTrac; TWO SEC VERSION!     */
/********************************************************************/

#include <math.h>

/* globals */

int genflg, vernflg, zazflg;	/* general use global flag-integer, vernier flg */
extern int pcflg; 
double latd, longd, latr, longr, pi, minf, secf, secrad, sidsrad;
double dtor, rtod, htod, coslat, sinlat, rsazmd, rsaltd, ppdazm, ppdalt;
double hazmd, haltd, tazmd, taltd;
extern double azmd, altd, pdecr, phar, azmcd, altcd;

void s_gconst()
{
 latd =  44.81588891;	/* Beaver Creek Latitude, fixed 2/9/97 */
 longd = 91.27183333;	/* longitude */
 pi = 3.14159256;
 minf = 16.66666666E-3;
 secf = 277.77777777E-6;
 dtor = pi/180.0;
 rtod = 180.0/pi;
 htod = 360.0/23.93447219;
 secrad = secf*htod*dtor;
 sidsrad = secrad * 1.0027379093; 
 
 genflg = 0;
 vernflg = 0;
 zazflg = 0;	/* flag to toggle 0, -1, for ZeroAZM problem */ 
 pcflg = 0;
 azmcd = 0.0;	/*zero corrections @ init */
 altcd = 0.0;

 latr = latd*dtor;
 longr = longd*dtor;
 coslat = cos(latr);
 sinlat = sin(latr);
					/* note: this is TWO SEC trk version */
 rsazmd = 6987.0/720.0; 		/* rev/sec factor per azm degree */
 rsaltd = 1400.0/720.0;                 /* rev/sec factor per alt degree */
 ppdalt = -(25000.0 * 1400.0)/360.0;	/* pulses per +degree altitude */
 ppdazm = -(25000.0 * 6987.0)/360.0;	/* pulses per +degree azimuth */  

 hazmd =  0.0;		/* home pos, azimuth, degrees */
 haltd =  0.0;		/* home pos, altitude, degrees */

 tazmd = hazmd;				/* telescope current azimuth deg */
 taltd = haltd;				/* telescope current altitude deg */

 azmd = hazmd;				/* display azm */
 altd = haltd;				/* display alt */
 phar = hazmd * dtor;			/* display hour angle */
 pdecr = (latd - 90.0) * dtor;		/* display declination */
}


	
                                                