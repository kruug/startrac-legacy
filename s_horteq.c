/********************************************************************/
/*				S_HORTEQ.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Horizon to Equatorial Calculations 		    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-12-97 				    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*	Used by routines m_mazalt() & m_mjoyst() to derive ha & dec */
/*	from altitude and azimuth.                                  */
/********************************************************************/

#include <math.h>

extern double altd, azmd, phar, pdecr, dtor, sinlat, coslat, pi;
extern double cosd, k1, k2, k3, k4;  /** add k5 later */

void s_horteq(void)
{
 double caltr, cazmr, cosaltr, sinaltr, cosazmr, cdecr, cmhar, sind;
 double coranr, paltcr, pazmcr, kqtst;

 caltr = altd * dtor;
 cazmr = azmd * dtor;
 
 /*remove platfrm fixes*/
 coranr = cazmr - 1.0472;	/* corr angle */
 paltcr = -0.0020944 * sin(coranr);  /* alt cor */
 pazmcr = -0.00200 * cos(coranr)/cos(caltr);  /* azm cor */
 caltr -= paltcr;	/* alt cor removed */
 cazmr -= pazmcr;	/* azm cor removed */

 cosaltr = cos(caltr);
 sinaltr = sin(caltr);
 cosazmr = cos(cazmr);
 
 cdecr = asin(-coslat*cosaltr*cosazmr + sinlat*sinaltr);
 if (cdecr > 1.57079 && cdecr < 1.57081)   /* near N pole */
  { 
   cmhar = 0.0;
   cosd = 0.0000001;
  }
 else 
  {
   cmhar = asin(cosaltr*sin(cazmr)/cos(cdecr));
   cosd = cos(cdecr);	/* to global */
  }

 sind = sin(cdecr);
 k1 = coslat*cosd; 	/* EQ parm's to globals */
 k2 = sinlat*sind;
 k3 = coslat*sind;
 k4 = sinlat*cosd;
 /** k5 = coslat/cosd; **/

 kqtst = sinlat * cosaltr * cosazmr + coslat * sinaltr;
 if(kqtst < 0.00) cmhar = pi - cmhar;	/* correct quadrant */ 
 
 phar = cmhar; 
 pdecr = cdecr;

 return;
}


	
                                                