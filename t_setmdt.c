/********************************************************************/
/*				T_SETMDT.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Set Time, Date (for Startrac)    		    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-16-97 				    */
/*	LAST UPDATE:	6-16-97  				    */
/*------------------------------------------------------------------*/
/*	Set time t_settim() , or set date t_setdat().		    */
/*      Then call sidtime, cadisp & return to caller                */
/********************************************************************/

#include <stdlib.h>
#include <dos.h>
#include <string.h>
#include "window.h"
#include "field.h"
#include "keyboard.h"
#include "stflib.h"

/***Set Time **/

void t_settim(void)
{
 int key, hours, minutes, seconds;
 char tbuffer[10], tmpbuf[10];
 struct time timebuf;
 Window wtim; 
 Field fd;	
 char tmprompt[]   = "Current Time (HR:MN:SC) ";
 char ttemplat[] = "__:__:__";
 int  trange = NUMERIC; 

 wtim = w_open(24,16,34,3);
 w_umessage(wtim, "  Current Time Entry Window  ");
 w_lmessage(wtim, "  press Return, on WWV Tone  ");
 
 fd = f_create(tmprompt, ttemplat);
 f_range(fd,trange);
 f_return(fd, ENABLE);
 f_setbuffer(fd, "      ");	/* all spaces */
 key = f_process(wtim,1,1,fd);
 f_getmasked(fd,tbuffer);
 if(key != _ESC)    
  {
   if(tbuffer[1] != ' ')     /* assume ok entry if not a 'space' here */
    {
     strncpy(tmpbuf, tbuffer, 2);
     hours = atoi(tmpbuf);          
     strncpy(tmpbuf, tbuffer, 5);
     strnset(tmpbuf, ' ', 3);
     minutes = atoi(tmpbuf);
     strnset(tbuffer, ' ', 6);
     seconds = atoi(tbuffer);
     timebuf.ti_hour = (unsigned char) hours;
     timebuf.ti_min = (unsigned char) minutes;
     timebuf.ti_sec = (unsigned char) seconds;
     timebuf.ti_hund = 0;
     settime(&timebuf);
     t_sidtim();	/* calls sidtim which calls cadisp */
    }
  }     
 w_close(wtim);
 f_free(fd);  
 return;
}

/*** Set Date ***/

void t_setdat(void)
{
 int key, month, day, year;
 char dbuffer[10], tmpbuf[10];
 struct date datebuf;
 Window wdat; 
 Field fd;	
 char dtprompt[]   = "Todays Date (MM/DD/YY) ";
 char dtemplat[] = "__/__/__";
 int dtrange = NUMERIC; 

 wdat = w_open(24,16,34,3);
 w_umessage(wdat, "  Todays Date  Entry Window  ");
 w_lmessage(wdat, " press Return, when finished ");
 
 fd = f_create(dtprompt, dtemplat);
 f_range(fd,dtrange);
 f_return(fd, ENABLE);
 f_setbuffer(fd, "      ");	/* all spaces */
 key = f_process(wdat,1,1,fd);
 f_getmasked(fd,dbuffer);
 if(key != _ESC)    
  {
   if(dbuffer[1] != ' ')     /* assume ok entry if not a 'space' here */
    {
     strncpy(tmpbuf, dbuffer, 2);
     month = atoi(tmpbuf);          
     strncpy(tmpbuf, dbuffer, 5);
     strnset(tmpbuf, ' ', 3);
     day = atoi(tmpbuf);
     strnset(dbuffer, ' ', 6);
     year = atoi(dbuffer);
     datebuf.da_mon = (char) month;
     datebuf.da_day = (char) day;
     datebuf.da_year = 1900 + year;
     setdate(&datebuf);
     t_sidtim();	/* calls sidtim which calls cadisp */
    }
  }     
 w_close(wdat);
 f_free(fd);  
 return;
}

                                                                   