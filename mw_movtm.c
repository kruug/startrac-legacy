/********************************************************************/
/*				MW_MOVTM.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Startrak Move Track (true) Menu Version		    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	5-24-97 				    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*								    */
/********************************************************************/
#include <stdlib.h>
#include "window.h"
#include "menu.h"
#include "defaults.h"
#include "stflib.h"

extern int dsmode;

mw_movtm()
{
 int i,j,menu_choice;
 Menu  mnmtr;
 Menu_Item  items[6];
 char *MTtitle =       " STARTRAC MOVE/TRACK MENU ";
 char *MTnames[6] = {  "  Move to ~RA/DEC & TRACK  ",
                       "  Move Via ~GUIDE & TRACK   ",
                       "  Move to ~AZ/ALT & HOLD   ",
                       "  Move to ~HOME & PARK     ",
                       "  Move Via ~JOYSTICK Ctrl  ",
                       "  Return to ~Opening Menu  " };
 
 dsmode = 1; /* time upd only for now */

 do
  {
   mnmtr = m_create(MTtitle,27,14,26,6);
   for(i=0; i<6; i++)   items[i] = m_additem(mnmtr, MTnames[i]);
   m_open(mnmtr);
   menu_choice = m_lastindex(mnmtr);
   switch(menu_choice)
    {
     case 0: { m_free(mnmtr); m_mradec(0); break; }
     case 1: { m_free(mnmtr); m_mradec(3); break; }
     case 2: { m_free(mnmtr); m_mazalt();  break; }
     case 3: { m_free(mnmtr); m_mhompk();  break; }
     case 4: { m_free(mnmtr); m_mjoyst();  break; }
     case 5: { m_free(mnmtr); break; }
    }
  } while(menu_choice != 5);
  return;  
}
