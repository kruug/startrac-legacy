/********************************************************************/
/*				M_MJOYST.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Move Telescope Via Joystick			    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-25-97 				    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*								    */
/*								    */
/*								    */
/********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "window.h"
#include "defaults.h"
#include "keyboard.h"
#include "stflib.h"

extern int hmsflg, pcflg, dlfs, sidhr, sidmin;
extern char instr[], dlgbuf[], ltimstr[];
extern int trkflg, vazmct, valtct;
extern double tazmd, azmd, altd, taltd, ppdazm, ppdalt, phar, pdecr;
extern double rtod, csidtm, sidsec;	

void m_mjoyst (void)
{
 int i, sisec;
 int btrkflg, key, jflg, pen_color1, pen_color2, pen_color3, rv;
 char *stptr, rplystr[10], tmpbuf[20], modstr[4];
 double jmvazm, jmvalt;		/* joystick move in azm, alt (dp) */
 long   lmvazm, lmvalt;		/* same long int */

 Window wnjoy;

 pen_color1 = d_colorattr(RED,BLUE,ENABLE,DISABLE);
 pen_color2 = d_colorattr(WHITE,BLUE,DISABLE,DISABLE); 
 pen_color3 = d_colorattr(RED,WHITE,DISABLE,DISABLE);
 
 jflg = 0;
 do 
  {
   tc_smode(3);		/* enable Joystick */
   tc_smode(8); 	/* joystick status */
   stptr = strchr(instr, 'J');	/* look for 'J' in 1SJ<>1xx0<> of reply */
   if((*(stptr+2)=='1') && (*(stptr+5)=='0'))
       jflg = 1;  /*OK response*/
   else jflg += 2;
   if(jflg == 2) wn_gmesg(6);	/* message to "turn on and try again" */
   else if(jflg == 4) { tc_smode(4);  return; }
                        	/* exit on 2nd failure */
  } while(jflg != 1);

  /** joystick OK, put up display of what to do **/
 wnjoy = w_open(11, 14, 58, 4);
 w_umessage(wnjoy, " Joystick Now Enabled for Telescope Positioning ");   
 w_poscurs(wnjoy,0,1);
 w_printf(wnjoy, " To Begin TRACKING  Select ");
 w_pencolor(wnjoy, pen_color2);
 w_printf(wnjoy, "'DISABLE'");
 w_pencolor(wnjoy,pen_color1);
 w_printf(wnjoy, " Function @ Joystick\n");
 w_printf(wnjoy, "                      or,  press  ");
 w_pencolor(wnjoy,pen_color2);
 w_printf(wnjoy, "Space Bar \n");
 w_pencolor(wnjoy, pen_color1);
 w_printf(wnjoy, "    Else,  To Return Back to the MENU  press ");
 w_pencolor(wnjoy, pen_color2);
 w_printf(wnjoy, "ESCape ");
 w_pencolor(wnjoy, pen_color1);
 
 do
  { 
   btrkflg = 0;
   key = 0;
   if(hmsflg == 1)
    {
     hmsflg = 0;
     tc_smode(8);		/* read joy stat */
     stptr = strchr(instr, 'J');
     if((*(stptr+2)=='0') && (*(stptr+3)=='1')) 
          btrkflg = 1; /*disabled*/
    }  
   if(k_ready())
    {
     key = k_getkey();
     if(key == ' ')  btrkflg = 1;	/* space bar key */
    }
  } while(key != _ESC && btrkflg == 0);

 if(btrkflg == 0)	/* Escape */
  {
   tc_smode(4); 	/* kill joystick */
   w_close(wnjoy);
   return;
  }
 else    	/* btrkflg==1, begin tracking fcn */
  {
   tc_smode(4);		/* kill Joystick */
   w_close(wnjoy);
   wnjoy = w_open( 17, 13, 46, 4);
   w_umessage(wnjoy, " Joystick Selected Telescope Position ");
   tc_smode(9);		/* get 1X1 */
   stptr = strchr(instr, 'X');
   for(i=0; i<9; i++) 
    {     
     rplystr[i] = *(stptr+3+i);
    } 
   rplystr[9] = '\0';
   lmvazm = atol(rplystr);	/* long int value of move pulses */   
   jmvazm = lmvazm;		/* dp value of move pulses */
   tc_smode(10);	/* get 2X1 */
   stptr = strchr(instr, 'X');
   for(i=0; i<9; i++)
    {
     rplystr[i] = *(stptr+3+i);
    }
   rplystr[9] = '\0';
   lmvalt = atol(rplystr);
   jmvalt = lmvalt;
   
   /* new telescope position after move is */
   tazmd += jmvazm/ppdazm;
   if(tazmd < 0.0) tazmd += 360.0;	/* make + */
   if(tazmd >= 360.0) tazmd -= 360.0;	/* make angle between 0 & 359.9 */
   taltd += jmvalt/ppdalt;
   
   w_pencolor(wnjoy, pen_color2);	/* white on blue */  
   w_poscurs(wnjoy,0,1);
   w_printf(wnjoy, "  The AZIMUTH  Angle is %f deg \n", tazmd);
   w_printf(wnjoy, "  The ALTITUDE Angle is %f deg \n", taltd); 
   w_pencolor(wnjoy, pen_color1);

   altd = taltd;
   azmd = tazmd;
   s_horteq();	/* note: platfrm corr implicitly incl in spotted item */

 
    /** additions for d_logger **/
     if(dlfs==1)
      {
        sprintf(modstr, "JOY"); 
        sprintf(tmpbuf, "START ");
        strcpy(dlgbuf, tmpbuf);
        strcat(dlgbuf, ltimstr);	/*local time */
        sisec = sidsec;  /* converts double to int */
        sprintf(tmpbuf, "; %s;ST%2d:%2d:%2d; ", modstr,sidhr, sidmin, sisec);
        strcat(dlgbuf,tmpbuf);        
        strset(tmpbuf, '\0');
        sprintf(tmpbuf,"***JOYSTICK***"); 
        strcat(dlgbuf,tmpbuf);
        sprintf(tmpbuf,"; HA%9.5f d ; ", phar * rtod);
        strcat(dlgbuf, tmpbuf);          
        sprintf(tmpbuf,"DE %9.5f d;\n",pdecr * rtod );
        strcat(dlgbuf, tmpbuf); 
      }
        /** end of first d_logger segment **/



           /* set up for tracking */
   trkflg = 0;  vazmct = 0; valtct = 0;
   pcflg = 1;	/* pltfrm cor = 1 , note */
   rv = m_trackf();	/* first pass */
   if(rv < 0)	/* abort tracking from initial cond */
    {
     if(rv ==-1)  wn_gmesg(3);	/* excessive rates */
     else if(rv ==-2)  wn_gmesg(1); /* below horizon */
     trkflg = 0;  /* precaution */   
     tc_smode(5); /* stop motors, precaution*/
     w_close(wnjoy);
     return;
    }
   
          /** else tracking in progress **/
   rv = wn_trkms(2);	/* kb control */
       /* @ return from above stop tracking & return to menu */
   trkflg = 0;
   tc_smode(11);	/* stop motors w/rptbk */
   if(rv == -1)  wn_gmesg(3);  /* excessive rates */
   else if(rv == -2) wn_gmesg(1);	/* below horiz */
   m_rdtpos(0);		/* get ts pos */
  

   /*** final d_logger section **/
   if(dlfs == 1)
    {
     sisec = sidsec;
     sprintf(tmpbuf, "END:ST%2d:%2d:%2d; ", sidhr, sidmin, sisec);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "AZ %9.5f d; ", azmd);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "ALT %8.5f d; ", altd);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "HA %9.5f d; ",  phar * rtod);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "DE %9.5f d; \n\n", pdecr * rtod);
     strcat(dlgbuf, tmpbuf);
    }

   w_close(wnjoy);
   if(dlfs==1) d_logger(1);	/*write disk*/
   return;
  }       
}
 


