/********************************************************************/
/*				MW_VERNR.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Startrak Modify Vernier Rates 			    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	7-24-97 				    */
/*	LAST UPDATE:    11-30-97  				    */
/*------------------------------------------------------------------*/
/*								    */
/********************************************************************/
#include <stdlib.h>
#include "window.h"
#include "menu.h"

extern int vernflg;

mw_vernr()
{
 int i,menu_choice;
 Menu  mnvern;
 Menu_Item  items[4];
 char *MVtitle =       "MENU - SELECT VERNIER RATE";
 char *MVnames[4] = {  "  Vernier Rate,+- ~One Pixel   ",
                       "  Vernier Rate,+- ~Ten Pixels  ",
                       "  Vernier Rate,+- ~40 Pixels   ",
                       "  Vernier Rate,+- ~100 Pixels  " };


   mnvern = m_create(MVtitle,25,19,30,4);
   for(i=0; i<4; i++)   items[i] = m_additem(mnvern, MVnames[i]);
   m_open(mnvern);
   menu_choice = m_lastindex(mnvern);
   switch(menu_choice)
    {
     case 0:  vernflg = 0; break; 
     case 1:  vernflg = 1; break; 
     case 2:  vernflg = 2; break; 
     case 3:  vernflg = 3; break; 
    }
  m_free(mnvern);
  return;  
}
