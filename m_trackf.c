/********************************************************************/
/*			    M_TRACKF.C  			    */
/*------------------------------------------------------------------*/
/*   	TASK:  Tracking Function (for Startrac)  TWO SEC VERSION    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-10-97 				    */
/*	LAST UPDATE:	11-30-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: nothing, all variables needed are globals,       */
/*      global azmd angle assumed to be positive.		    */ 
/*      Returns: 0 if OK, -1 if tracking rates excessive (abort),   */
/*      -2 if move is to below horizon (aborts).		    */
/********************************************************************/

#include <math.h>
#include "stflib.h"

int trkflg, vazmct, valtct;  /* defined here, globals-track flag & counts */
int pcflg; 	/* defined here pltfrm cor flg  */
extern int genflg, vernflg;
extern double altd, azmd, phar, cosd, k1, k2, k3, k4;
extern double sidsrad, rtod, rsazmd, rsaltd;

int m_trackf(void)
{
 
 double nwaltr, nwaltd, nwazmr, nwazmd, nwphar, cosnha, kqtst, cosnalt; 
 double deltazmd, deltaltd, dblarg, coranr, paltcr, pazmcr;
 float  vazm, valt, vvrnazm[4], vvrnalt[4], vdangd[4];
 static double acumdazm, acumdalt; 
 static union mc_info mctrkb;

 /** following vernier velocities & angles for two second track routine **/
 vvrnazm[0] = 0.00418;  vvrnazm[1] = 0.04172;  vvrnazm[2] = 0.16688;
 /* vvrnazm[3] = 0.00104;  this was 0.25 pixel */
 vvrnazm[3] = 0.41720;	/* test 100 pixel delta */
 vvrnalt[0] = 0.00084;  vvrnalt[1] = 0.00836;  vvrnalt[2] = 0.03344;
 /* vvrnalt[3] = 0.00022; this was 0.25 pixel */
 vvrnalt[3] = 0.08360; /* test 100 pixel delta */

 /** the following vdangd[] angles are not currently being used, but save **/
 vdangd[0]  = 0.00043;  vdangd[1]  = 0.00430;  vdangd[2]  = 0.01720;
 /* vdangd[3]  = 0.00011; this was 0.25 pixel */
 vdangd[3] = 0.04300;	/* test 100 pixels */

 if(trkflg==0)	   /* first entry to routine for THIS tracking operation */
  {
   trkflg = 1;	/* the ONLY place where this global is SET */
   acumdazm = 0.0;  
   acumdalt = 0.0;
   mctrkb.all = 0;
   tc_smode(2);		/* set up  mtr state for 'MC' */
  }

  nwphar = phar + sidsrad * 2.0;	/* two second track routine */
  cosnha = cos(nwphar);
  kqtst = k4*cosnha - k3;
  nwaltr = asin(k1*cosnha + k2);
  nwazmr = asin(cosd*sin(nwphar)/cos(nwaltr));
  cosnalt = cos(nwaltr);   /* used in divisors, platfm fix & vernier rates */
  if(cosnalt < 0.2) cosnalt = 0.2;	/* not smaller than this! */

  /* platform fixes */
  if(pcflg==1)  
   {
    coranr = nwazmr - 1.0472;
    paltcr = -0.0020944 * sin(coranr);
    pazmcr = -0.00200 * cos(coranr)/cosnalt;
    nwaltr += paltcr;   /* corrected value */
    nwazmr += pazmcr;	/* corrected value */
   }
 
  nwaltd = nwaltr * rtod;
  if(nwaltr <= 0.0) /* bail out, below horizon */
     { trkflg = 0; genflg = 2; return -2; }
       /* wn_trk senses, move routine does "below horiz", stops tscope, etc */
  nwazmd = nwazmr * rtod;
  if(kqtst<0) nwazmd = 180.0 - nwazmd;	/* proper segment */
  if(nwazmd < 0) nwazmd += 360.0;	/* make pos angle */

  deltaltd = nwaltd - altd;
  deltazmd = nwazmd - azmd;
  if(deltazmd < -180.0 ) deltazmd +=360.0;  /* fix for 359.9 to 0.1 region */

     /* Following to test for small delta's */   
         
  if(deltazmd < 0.0)  dblarg = -deltazmd;   /* make + */
  else dblarg = deltazmd;
  if(dblarg < 0.0000025)	/* acum small values */
   {
    acumdazm +=deltazmd;
    if(acumdazm > 0.0000052)  { vazm = 0.0001; acumdazm = 0.0; }
    else if(acumdazm < - 0.0000052) { vazm =-0.0001; acumdazm = 0.0; }
    else vazm = 0.0;
   }
  else vazm = deltazmd * rsazmd;
         
  if(deltaltd < 0.0) dblarg = -deltaltd;
  else dblarg = deltaltd;
  if(dblarg < 0.000012)		/* acum */
   {
    acumdalt +=deltaltd;
    if(acumdalt > 0.000026)  { valt = 0.0001; acumdalt = 0.0; }
    else if(acumdalt < -0.000026) { valt =-0.0001; acumdalt = 0.0; }
    else valt = 0.0;
   }
  else valt = deltaltd * rsaltd;
        
  
  /* vernier operations */
  if(vazmct > 0)  { vazm += vvrnazm[vernflg]/cosnalt;  vazmct--; } 
  if(vazmct < 0)  { vazm -= vvrnazm[vernflg]/cosnalt;  vazmct++; }
  if(valtct > 0)  { valt += vvrnalt[vernflg];  valtct--; }
  if(valtct < 0)  { valt -= vvrnalt[vernflg];  valtct++; }   
      

       /* test for excessive velocity */
  if(vazm > 2.0000 || valt > 2.0000) { trkflg = 0; genflg = 1; return -1; } 
  /* wn_trk senses,move rout calls for message "excessive tracking rates" */  
       /* and also "stops" motors & cleans up the environment */

      /* direction tests & make velocity # positive */
   if(mctrkb.part.init_con == 0)	/* initial set up */
    {
     mctrkb.part.init_con = 1;
     mctrkb.part.ax1_chng = 1;
     mctrkb.part.ax2_chng = 1;
     if(vazm < 0.0)
      { mctrkb.part.ax1_cdir = 1;
        mctrkb.part.ax1_pdir = 1;
        vazm = - vazm;
      }
     if(valt < 0.0)
      { mctrkb.part.ax2_cdir = 1;
        mctrkb.part.ax2_pdir = 1;
        valt = - valt;
      }
    }
   else		/* non initial, normal path */
    {
     if(vazm < 0.0)
      { mctrkb.part.ax1_cdir = 1;
        if(mctrkb.part.ax1_pdir == 0) mctrkb.part.ax1_chng = 1;
        else mctrkb.part.ax1_chng = 0;
        mctrkb.part.ax1_pdir = 1;
        vazm = - vazm;
      }
     else	/* vazm >= 0.0 */
      { mctrkb.part.ax1_cdir = 0;
        if(mctrkb.part.ax1_pdir == 1) mctrkb.part.ax1_chng = 1;
        else mctrkb.part.ax1_chng = 0;
        mctrkb.part.ax1_pdir = 0;
      }
     if(valt < 0.0)
      { mctrkb.part.ax2_cdir = 1;
        if(mctrkb.part.ax2_pdir == 0) mctrkb.part.ax2_chng = 1;
        else mctrkb.part.ax2_chng = 0;
        mctrkb.part.ax2_pdir = 1;
        valt = -valt;
      }
     else	/* valt >= 0.0 */
      { mctrkb.part.ax2_cdir = 0;
        if(mctrkb.part.ax2_pdir == 1) mctrkb.part.ax2_chng = 1;
        else mctrkb.part.ax2_chng = 0;
        mctrkb.part.ax2_pdir = 0;
      }
    }  
       
      /* send MC command to motors */
  tc_movec(mctrkb.all, vazm, valt);
    /*  'GO' & re-loop m_trackf done from t_osectc routine if trkflg==1 */
        
  altd = nwaltd;   /* update & save new values */
  azmd = nwazmd;
  phar = nwphar;
  return 0;	/* normal return */   

}
