/********************************************************************/
/*			    M_MRADEG.C  (temp with Guide ver)	    */
/*------------------------------------------------------------------*/
/*   	TASK: Move to RA & DEC Routine (Startrac)  TWO SEC VERSION  */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	10-5-96   ( core from my STRPOINT.C ) 	    */
/*	LAST UPDATE:	11-30-97  DEG				    */
/*------------------------------------------------------------------*/
/*	Passed to: mode 0 = move & track, mode 1 = move & hold,     */
/*                 mode 3 = get move command from GUIDE & track.    */
/*      Returns:  0 if OK, -1 if problem.     			    */
/*      Console Inputs: Objects NAME, RA & DEC                      */
/*      Guide Inputs: RA & DEC                                      */
/*      11/30 ver incl's fix for Alt Platform Correction, but Syst  */
/*      Coord Correction removed (temporarily                       */
/********************************************************************/

#include <stdio.h>
#include <math.h>
#include "window.h"
#include "field.h"
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <bios.h>
#include "stflib.h"
 
#define TXRDY(status3) ((status3 & 0x2000) == 0x2000)
#define RXRDY(status3) ((status3 & 0x0100) == 0x0100)

double altd, azmd, pdecr, phar, cosd, k1, k2, k3, k4;  /* defined here */
extern int trkflg, pcflg, vazmct, valtct, sidhr, sidmin, dlfs, sgdflg; 
extern char dlgbuf[], ltimstr[];
extern double latd, minf,secf;
extern double dtor, rtod, htod, coslat, sinlat;
extern double csidtm, sidsec, tazmd, taltd, rsazmd, rsaltd, ppdazm, ppdalt;

int m_mradec(int mode)
{
 char iname[20], indec[20], inra[20], tcbuf[16], cmdstr[36];
 char tmpbuf[20], modstr[4];
/** char cmpdate[10], tmpst[2], hcpy[2], dtc[2]; **/
/** int dtdur, phdr;**/

 char *prompt[] =   { " Object's Name/Description       ",
                      " Object's Right Ascension        ",
                      " Object's Declination            " };
  
 char *template[] = { "__________________",
                      "  __hrs__min__sec ",
                      " ___deg__min__sec " };

 int  range[] = { GRAPHIC,BLANK|NUMERIC, BLANK|SIGN|NUMERIC };

 char gokrsp[] = { '1', '\0'};	/* Note: Guide notes are wrong! */
 char gngrsp[] = { '0', '\0'};  /*       These are CORRECT!     */

 Window   winput, wsmsg;
 Field fd[3];
 struct time dtmbuf;

 int i, j, k, ect, rv, btkey;
 int status1, status2, status3;
 int  cprah, cpdcd, cpram, cpdcm, gdecd, grah;
 int mpassn, tsmvgen, ttstm1, corc, sisec;
 long lpmazm, lpmalt;
 float cpras, cpdcs, grafm, gdecfm;
 double prah, phah, pdecd, pstime, diffld, coranr, paltcr, pazmcr;
 double sind, cosha, cosnha, sinha, altr, azmr, kqtst;
 double tsmvazm, tsmvalt, dmvazmd, dmvaltd, pdblarg, ppmazm, ppmalt, msidtm; 

 if(mode == 0) sprintf(modstr, "RDT");
 if(mode == 1) sprintf(modstr, "RDH");
 if(mode == 3) sprintf(modstr, "RDG");

 /* for mode 3 = GUIDE operation */
 if(mode == 3)		/* Command from GUIDE */
  {
   status3 = bioscom(3,0,0);	/* read UART status from GUIDE CPU */
   if(RXRDY(status3))  status2 = bioscom(2,0,0);  /* flush inp buffr */

   winput = w_open(10,13,60,3);
   w_printf(winput, "       WAITING for RA/DEC Steering Command from GUIDE \n");
   w_printf(winput, "         --press any key to Exit & Return to Menu--  ");
   
   i = 0;
   ect = 0;
   strset(cmdstr, ' ');
   do
    {
     status3 = bioscom(3,0,0);
     if(RXRDY(status3))
      {
       status2 = bioscom(2,0,0);	/* read char from GUIDE */
       cmdstr[i++] = status2;
       if(ect > 0) ect++;
       if(status2 == 'M') ect = 1;
      }
    } while (!kbhit() && ect < 3);	/* until end of string or key press */
 
   if(kbhit()) { getkey(); w_close(winput); return -1; }  /* return to menu */

    /* else process GUIDE command */
   j = 0;
   k = 0;
   while(cmdstr[j] != 'r') j++;		/* looking for 'r' */
   j++;
   strset(tcbuf,' ');   
   while(cmdstr[j] != ':') tcbuf[k++] = cmdstr[j++];  /* get RA hrs */
   grah = atoi(tcbuf);
   j++;
   k = 0;
   strset(tcbuf,' ');
   while(cmdstr[j]  != '#') tcbuf[k++] = cmdstr[j++];	/* get RA min */
   grafm = atof(tcbuf);
   while(cmdstr[j] != 'd') j++;
   j++;
   k = 0;
   strset(tcbuf,' ');
   while(cmdstr[j] != 223) tcbuf[k++] = cmdstr[j++];	/* get DEC deg */
   gdecd = atoi(tcbuf);
   j++;
   k = 0;
   strset(tcbuf,' ');
   while(cmdstr[j] != '#') tcbuf[k++] = cmdstr[j++];	/* get DEC min */
   gdecfm = atof(tcbuf);

   /* display Guide command */
   w_close(winput);
   winput = w_open(8, 13, 64, 4);
   w_umessage(winput, " GUIDE Move/Track Command Parameters ");
   w_poscurs(winput,0,1);
   w_printf(winput, " Object's Right Ascension  %.2i hours,   %5.2f minutes\n"
                       , grah, grafm);
   w_printf(winput, " Object's Declination      %.2i degrees, %5.2f minutes\n"
                       , gdecd, gdecfm);
   
   /* combine info to fp decimal format  */
   prah = grah + grafm * minf;
   if(gdecd < 0)  pdecd = gdecd - gdecfm * minf;	/* negative dec */
   else  pdecd = gdecd + gdecfm * minf;	        /* positive dec */

  }
 else 	/* it is mode 1 or mode 2 */
  {

   winput = w_open(8, 12, 64, 5);	/* input window */
   if(mode==0)
     w_umessage(winput, " MOVE TO RA/DEC and TRACK - INPUT COORDINATES ");
   else if(mode==1)
     w_umessage(winput, " MOVE TO RA/DEC and HOLD - INPUT COORDINATES  ");
   w_printf(winput, "  Ranges: Declination (0 to +-90 deg),  RA (0 to 23h 59m 59.9s) \n");
   for(i=0; i<3; i++)
    {
     fd[i] = f_create(prompt[i], template[i]);
     f_range(fd[i], range[i]);
     f_append(winput, 5, i+1, fd[i]);
    }
   w_poscurs(winput,0,4);
   w_printf(winput,"      ---press RETURN after all data has been entered---");
   f_return(fd[2], ENABLE);
   btkey = f_batch(winput);
   w_poscurs(winput,0,4);
   w_printf(winput,"                                                        ");
   f_getmasked(fd[0], iname);
   f_getmasked(fd[1], inra);
   f_getmasked(fd[2], indec);
 
   for(i=0;i<3; i++) f_free(fd[i]);

   if(btkey == 27 || btkey == -1)
     { w_close(winput); return -1; }  /* on ESCape key or ERROR CODE of -1 */

   /*****process Kb Inputs *********/
   strset(tcbuf,' ');
   strncpy(tcbuf,inra,4);		/* ra */
   cprah = atoi(tcbuf);
   strset(tcbuf,' ');
   strnset(inra, ' ', 7);
   strncpy(tcbuf, inra, 9); 
   cpram = atoi(tcbuf);
   strset(tcbuf,' ');
   strnset(inra, ' ', 12);
   cpras = atof(inra);

   strset(tcbuf,' ');
   strncpy(tcbuf,indec,4);		/* dec */
   cpdcd = atoi(tcbuf);
   strset(tcbuf,' ');
   strnset(indec, ' ', 7);
   strncpy(tcbuf, indec, 9);
   cpdcm = atoi(tcbuf);		
   strset(tcbuf,' ');
   strnset(indec, ' ',12);
   cpdcs = atof(indec);

   /* convert to fp decimal */ 
   prah  = cprah + cpram * minf + cpras * secf;
   if(cpdcd < 0)  pdecd = cpdcd - cpdcm * minf - cpdcs * secf; /* negative*/
   else pdecd = cpdcd + cpdcm * minf + cpdcs * secf;  /* positive dec */

  }

   /*** added for display of prah & pdecd ***/
   w_poscurs(winput, 0, 4);
   w_printf(winput, "           prah = %f    pdecd = %f", prah, pdecd);
   /*** end added display ***/

   /*** Calculations - Equatorial to Horizon ***/
 
   trkflg = 0;  /* ensure no tracking @ start */

   diffld = latd - pdecd;	/* testing for forbidden dec */
   if(diffld < 0) diffld = -diffld; /* abs pos valu */
   if(diffld < 0.0004)		/* forbidden zone < 1.5 arc sec frm 'zero' */
    {
     if(mode == 3)	/* guide */
      {
       j = 0;
       do
        {
         status3 = bioscom(3,0,0);
         if(TXRDY(status3))
           status1 = bioscom(1,gngrsp[j++],0);	/* pos no good to guide */
        } while(gngrsp[j] != '\0');
      }
     
     rv = wn_gmesg(0);  /* forbidden declination message */ 
     w_close(winput);
     return -1;		/* return to M/T Menu */
    }  

   tc_smode(1); 	/** Send 'MN' set up to telescope mtrs */ 

   pdecr = pdecd * dtor;
   cosd = cos(pdecr);
   sind = sin(pdecr);
   k1 = coslat*cosd;
   k2 = sinlat*sind;
   k3 = coslat*sind;
   k4 = sinlat*cosd;

   /* note coarse pass (0,1) uses csidtm, sync-to-track pass(2) uses msidtm */
   /* if time to complete move, tmovs < 20 seconds then no 'coarse' move */
   /* assumptions:  telescope moves set to A1, 1V10, 2V6   */
       
   mpassn = 0;	/* @ first entry pass = 0 */
   do
    {
     if(mpassn==0 || mpassn==1) phar = (csidtm - prah)*htod*dtor; 
                 /* current stime */
     else if (mpassn==2) phar = (msidtm - prah)*htod*dtor; /* mstim @move */
     cosha = cos(phar);
     sinha = sin(phar);
     kqtst = k4*cosha - k3;     

      /* altr= asin(coslat*cosd*cosha + sinlat*sind) */
     altr = asin(k1 * cosha  + k2);
     azmr = asin((cosd * sinha)/cos(altr));
  
     /* platform correction */
     coranr = azmr - 1.0472;	/* AZM - 60 deg */
     paltcr = - 0.0020944 * sin(coranr); /* alt correction */
     pazmcr = - 0.00200 * cos(coranr)/cos(altr);  /* azm correction */
     altr += paltcr;	/* corrected value */
     azmr += pazmcr;	/* corrected value */

     altd = altr * rtod;
     azmd = azmr * rtod; 
     

     if(altd < 0.0 && mpassn==0)	/* object below horiz */
       {
        if(mode == 3)	/* guide */
         {
          j = 0;
          do
           {
            status3 = bioscom(3,0,0);
            if(TXRDY(status3))
             status1 = bioscom(1,gngrsp[j++],0);  /* pos no good to guide */
           } while(gngrsp[j] != '\0');
         }
        rv= wn_gmesg(1);	/* below horiz message & wait for key pr */
        w_close(winput);
        return -1;	/* return to menu */    
       }
     else if(mpassn == 0)   /* send guide OK & telescope slewing message */
       { 		    /* and send message to d_logger if reqd */
        if(mode == 3)	/* guide */
         {
          j = 0;
          do
           {
            status3 = bioscom(3,0,0);
            if(TXRDY(status3))
             status1 = bioscom(1,gokrsp[j++],0);   /* pos OK to guide */
           } while(gokrsp[j] != '\0');
         }
 
        /** additions for d_logger **/
        if(dlfs==1)
         {
          sprintf(tmpbuf, "START ");
          strcpy(dlgbuf, tmpbuf);
          strcat(dlgbuf, ltimstr);	/*local time */
          sisec = sidsec;  /* converts double to int */
          sprintf(tmpbuf, "; %s;ST%2d:%2d:%2d; ", modstr,sidhr, sidmin, sisec);
          strcat(dlgbuf,tmpbuf);        
          strset(tmpbuf, '\0');
          if(mode < 3) strncpy(tmpbuf, iname, 14);
          else sprintf(tmpbuf,"--------------"); 
          strcat(dlgbuf,tmpbuf);
          sprintf(tmpbuf,"; RA%9.5f h ; ", prah);
          strcat(dlgbuf, tmpbuf);          
          sprintf(tmpbuf,"DE %9.5f d;\n",pdecd );
          strcat(dlgbuf, tmpbuf); 
         }
        /** end of first d_logger segment **/

        wsmsg = w_open(23,20,34,3);
        w_printf(wsmsg, "             --WAIT--           \n");
        w_printf(wsmsg, " SLEWING TELESCOPE INTO POSITION \n");        
        /* close this window just before calling tracking */
       }

     if(kqtst < 0)   		/** correct azimuth segment **/
      {
       azmd = 180.0 - azmd;
      }
     if(azmd < 0) azmd += 360.0;	/* make positive angle */     
     
     /*** now calc move distance ***/
     dmvazmd = (azmd - tazmd); 
     if(dmvazmd >  180.0) dmvazmd -=360.0;   /* get move between +- 180 deg */
     if(dmvazmd < -180.0) dmvazmd +=360.0;
     dmvaltd = (altd - taltd);

     ppmazm = dmvazmd * ppdazm;   	/* pulses per move , pulses per deg */  
     ppmalt = dmvaltd * ppdalt;
    
     if(mpassn == 2 || mode == 1)  mpassn=3;	/* end of do loop */     
     else  /* on first, or after coarse pass */
      {			/* estmating time to do sync-to-track move */
       if(ppmazm < 0.0) pdblarg = -ppmazm;	
       else pdblarg = ppmazm;
       tsmvazm = sqrt(pdblarg/6250.0);  	/* ~time (sec) to move in azm */
       if(ppmalt < 0.0) pdblarg = -ppmalt;
       else pdblarg = ppmalt;
       tsmvalt = sqrt(pdblarg/6250.0);      /* ~time (sec) to move in alt */ 

       if(tsmvazm <= 16.0 && tsmvalt <= 10.0)  /*largest times w/o coarse mv*/ 
          /* actual max's are 20 & 12 for V10 & V6, but use smaller #'s */
          /* note:tsmvazm was < 20.0 when AZ V was 16; now AZ V is 10 */
        {
         mpassn = 2;	/*  do move without coarse pass (movepass# = 2) */
         if(tsmvazm  > tsmvalt) tsmvgen = tsmvazm + 0.5; /*rnd up halfway*/
         else tsmvgen = tsmvalt + 0.5; /* largest time to move -int seconds*/
         tsmvgen +=6;	/* caution: add 6seconds, reduce to 2 sec if OK  */
         /*  get time: time to start tracking = dos time + tsmvgen */
         /*  fix for odd/even second start later */
         gettime(&dtmbuf);
         /*  (tsmvgen + dtmbuf.ti_sec)%60 ,  time to start tracking */
         ttstm1 = (tsmvgen -1 + dtmbuf.ti_sec)%60;   /* time tstrk -1 sec */
         msidtm = csidtm + (tsmvgen * 0.000278538);
                   /*sidereal time @ end of move*/       
        		/* next recompute positons, with msidtm */ 
        }     
       else    /* needs 'coarse' move first */
        {      /* do coarse move here */
          w_poscurs(wsmsg, 0,2);
          w_printf(wsmsg, "  Making Initial (Coarse) Move ");
          lpmazm = ppmazm;  lpmalt = ppmalt;
          tc_moven(lpmazm, lpmalt);	/* move telescope *      
           /* later put test here for move verification */  
          tazmd += dmvazmd;  
          if(tazmd < 0.0) tazmd += 360.0;
          if(tazmd > 360.0) tazmd -= 360.0;
          taltd += dmvaltd;  /* t position after move */
          t_cadisp(1);	/* updates sidereal time, for next calculation */
          mpassn = 1; 
         }
      }
    } while (mpassn < 3);	/* end of alt/azm move comp do loop */
 

   /** Do Final Positioning Move Here **/ 
   w_poscurs(wsmsg, 0,2);
   w_printf(wsmsg, "       Making Final Move       ");
   lpmazm = ppmazm;  lpmalt = ppmalt;
   tc_moven(lpmazm, lpmalt);	/* move */
     /* later verify move here */
   tazmd += dmvazmd;  
   if(tazmd < 0.0) tazmd += 360.0;
   if(tazmd > 360.0) tazmd -= 360.0;
   taltd += dmvaltd;  /* t pos after move */
   t_cadisp(1);		/* update displays */

   /* if(mode 1) [move & hold] wait here for key press or exit key */
   /* else if(mode 0 or 3) [move & track] wait for 1sec EARLY tick & proceed */
   if(mode==0 || mode ==3)		/* wait for " go time " */
    {
     w_poscurs(wsmsg, 0, 2);
     w_printf(wsmsg, "  Timing Out to Track Function ");
     do
      {
       gettime(&dtmbuf);
      } while(dtmbuf.ti_sec != ttstm1);    
     w_close(wsmsg);
    }
   else if(mode==1)	/* holding for key press to strt trackng */
    {
     w_close(wsmsg);
     rv = wn_gmesg(4);   /* message, press key to start trk or exit */     
     if(rv == -1) 
      {  
       w_close(winput);
       return 0;	/* to menu */
      }
     /* else start trking */
    }

               /** set up for Tracking Operation **/
   trkflg = 0; pcflg = 1; vazmct = 0; valtct = 0; 
   rv = m_trackf();	/* first pass -  tracking loop calculations */
   if(rv < 0)	/* abort from initial m_trackf set up only */  
    {
     if(rv ==-1)  rv = wn_gmesg(3);    /* "excessive rates"  */
     else if(rv ==-2)  rv = wn_gmesg(1);  /* "below horizn" */
     trkflg = 0;  /* precaution */ 
     tc_smode(5);	/* not needed, just precaution */
                        /* note: no move yet from MC so no pos update here */
     w_close(winput);
     return 0;	/* return to menu */
    }

                 /** else tracking operation now in progress **/
  
   rv = wn_trkms(0);	/* message, control keys for tracking fcn */
      /* (0) controls ability of wn_trkms() to correct coord system */                     
      /* returns here when tracking is to be stopped */        

    /* at exit make sure trkflg = 0; tscope stopped; t pos updated */
   trkflg = 0;
   corc = 0;
   tc_smode(11);	/* stop motors w/rptbk */   
   if(rv ==-1)  wn_gmesg(3);	/* excessive tracking rates */
   if(rv ==-2)  wn_gmesg(1); 	/*below horizon */
   /** if(rv ==-3)  corc = 1;    coord corr - not implemented **/    
   if(corc == 0) m_rdtpos(0);	/* reads TS pos & updates pos'n parm's */
   else if(corc ==1) 
      { m_rdtpos(1);  wn_gmesg(8); } /* read TS & re-initialize coord sys */

   /*** final d_logger section **/
   if(dlfs == 1)
    {
     sisec = sidsec;
     sprintf(tmpbuf, "END:ST%2d:%2d:%2d; ", sidhr, sidmin, sisec);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "AZ %9.5f d; ", azmd);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "ALT %8.5f d; ", altd);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "HA %9.5f d; ",  phar * rtod);
     strcat(dlgbuf, tmpbuf);
     sprintf(tmpbuf, "DE %9.5f d; \n\n", pdecr * rtod);
     strcat(dlgbuf, tmpbuf);
    }
   w_close(winput);
  if(dlfs == 1) d_logger(1);	/*write disk*/ 
  return 0; 	/* return to menu */
}
   
                                                                                            