/********************************************************************/
/*				M_MHOMPK.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Move to Home & Park				    */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	7-5-97  				    */
/*	LAST UPDATE:	7-19-97  				    */
/*------------------------------------------------------------------*/
/*	Move Telescope to Home & Re-set position var's to home      */
/*								    */
/********************************************************************/

#include <stdio.h>
#include "window.h"
#include "stflib.h"
	
extern double hazmd, haltd, tazmd, taltd, ppdazm, ppdalt;
extern double azmd, altd, phar, pdecr, dtor, rtod, latd, sidsec;
extern int dlfs, sidhr, sidmin;
extern char dlgbuf[], ltimstr[];

void m_mhompk(void)
{

 int sisec;
 char tmpbuf[20], modstr[4];  
 double mazmd, maltd, ppmazm, ppmalt;
 long  lpmazm, lpmalt;

 Window wnhom;

 mazmd = tazmd - hazmd;
 maltd = taltd - haltd;

 if(mazmd >  180.0) mazmd -=360.0;	/* make min move */
 if(mazmd < -180.0) mazmd +=360.0;

 ppmazm = - mazmd * ppdazm;	/* pulses to move in AZM */
 ppmalt = - maltd * ppdalt;	/* same in ALT */ 

 /* later test for minimal moves & put in fix to prevent cable twist up */
 /* for minimal move in AZ, move is  < | 87,337,500 | pulses */
 /* for minimal move in ALT, move is < | 17,500,000 | pulses */
 /* MIMIMAL moves important since MAX D to motors is =< 99,999,9999 */
 
 lpmazm = ppmazm;
 lpmalt = ppmalt;

 wnhom = w_open( 20,15,40,2);
 w_printf(wnhom, "             ---WAIT---              \n");
 w_printf(wnhom, "  Slewing Telescope to HOME Position ");

 tc_smode(1);	/* normal move "set up " */

 tc_moven(lpmazm, lpmalt);

 tazmd = hazmd;		/* telescope @ home */
 taltd = haltd;
 azmd  = hazmd;
 altd  = haltd;
 phar  = hazmd * dtor;
 pdecr = (latd - 90.0) * dtor;

 /** d logger **/
 if(dlfs==1)
  {
   sprintf(modstr, "HOM");
   sprintf(tmpbuf, " HOME "); 
   strcpy(dlgbuf, tmpbuf);
   strcat(dlgbuf, ltimstr);	/*local time */
   sisec = sidsec;  /* converts double to int */
   sprintf(tmpbuf, "; %s;ST%2d:%2d:%2d; ", modstr,sidhr, sidmin, sisec);
   strcat(dlgbuf,tmpbuf);        
   strset(tmpbuf, '\0');
   sprintf(tmpbuf,"-----HOME-----"); 
   strcat(dlgbuf,tmpbuf);
   sprintf(tmpbuf,"; HA%9.5f h ; ", phar);
   strcat(dlgbuf, tmpbuf);          
   sprintf(tmpbuf,"DE %9.5f d;\n\n", pdecr*rtod );
   strcat(dlgbuf, tmpbuf); 
  }

 w_close(wnhom);
 if(dlfs==1) d_logger(1);	/*write disk*/
 return;
}

