/********************************************************************/
/*			    WN_GMESG.C  			    */
/*------------------------------------------------------------------*/
/*   	TASK: General Message Windows for Startrac    	            */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	6-9-97				 	    */
/*	LAST UPDATE:	10-2-97  				    */
/*------------------------------------------------------------------*/
/*	Passed to: message #.  Returns: key press code or 0	    */
/*                 			                            */
/********************************************************************/

#include "window.h"
#include "keyboard.h"
#include "defaults.h"
#include "stflib.h"

extern double azmcd, altcd;

int wn_gmesg(int msgn)
{

 Window wmsg;
 int key, rv, pen_color1, pen_color2, pen_color3;


 pen_color1 = d_colorattr(RED,BLUE,ENABLE,DISABLE);
 pen_color2 = d_colorattr(WHITE,BLUE,DISABLE,DISABLE);
 pen_color3 = d_colorattr(YELLOW,BLUE,ENABLE,DISABLE);
 w_pencolor(wmsg, pen_color1);

 switch(msgn)
  {
   case 0: 
    {
     wmsg = w_open(19,20,42,2);
     w_printf(wmsg, "  Object In Forbidden Declination Region\n");
     w_printf(wmsg, "         [press any key to exit]        ");
     k_getkey();
     w_close(wmsg);
     return 0;		/* return to M/T Menu */
    }  
    
   case 1:   
    {
     wmsg = w_open(19,20,42,2);
     w_printf(wmsg, "  Selected Object is Below the Horizon    \n");
     w_printf(wmsg, "         [press any key to exit]          ");
     k_getkey();
     w_close(wmsg);
     return 0;
    }

   case 2:    
    {  
     azmcd = 0.0;  altcd = 0.0;  /** zeroing  of azm & alt cor values **/
     wmsg = w_open(14,20,52,2);
     w_printf(wmsg, " The Coordinate Correction Values Have Been Zeroed \n");
     w_printf(wmsg, "       [press any key to return to menu]      ");
     k_getkey();
     w_close(wmsg);
     return 0;
    }

   case 3:
    {
     wmsg = w_open(16,20,48,2);
     w_printf(wmsg, "  Excessive Tracking Rates - Telescope Stopped\n");
     w_printf(wmsg, "       [press any key to return to menu]   ");
     k_getkey();
     w_close(wmsg);
     return 0;
    }

   case 4:
    {
     wmsg = w_open(21,20,38,2);
     w_printf(wmsg, " To Begin TRACKING - press ");
     w_pencolor(wmsg, pen_color2);
     w_printf(wmsg, "Space Bar \n");
     w_pencolor(wmsg, pen_color1);
     w_printf(wmsg, "     To EXIT - press ");
     w_pencolor(wmsg, pen_color2);
     w_printf(wmsg, "Escape Key ");
     w_pencolor(wmsg, pen_color1);
     key = k_getkey();
     if(key == _ESC)  rv = -1; 	/* Escape */
     else rv = 0;  /* go to tracking */
     w_close(wmsg);
     return rv;     
    }
   
  case 5:
   {
    wmsg = w_open(16,20,48,2);
    w_printf(wmsg, "   The CompuMotor 2100 Indexer has been Reset \n");
    w_printf(wmsg, "          [ press any key to continue ]      ");
    tc_smode(0); 	/** sends out cmini string to motors **/
    key = k_getkey();
    w_close(wmsg);
    return 0;
   }
 
  case 6:
   {
    wmsg = w_open(12,20,56,2);
    w_printf(wmsg, " Turn Joystick Power & Enable SWITHCES ON @ Telescope \n");
    w_printf(wmsg, "             [press any key when done]     ");
    key = k_getkey();
    w_close(wmsg);
    return 0;
   }

  case 7:
   {
    wmsg = w_open(16, 20, 48, 2);
    w_printf(wmsg, " Forbidden Altitude, Must be < 89.9997 degrees \n");
    w_printf(wmsg, "            [press any key to exit]       ");
    key = k_getkey();
    w_close(wmsg);
    return 0;
   }
  
  case 8:
   {
    wmsg = w_open(14, 20, 52, 2);
    w_printf(wmsg, " Corrections to Coordinates are now In The System \n");
    w_printf(wmsg, "          [press any key to continue]       ");
    key = k_getkey();
    w_close(wmsg);
    return 0;
   }
 }
}
