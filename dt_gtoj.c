/********************************************************************/
/*                              DT_GTOJ.C			    */
/********************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <conio.h>

static char indate[11];
static char gregdate[11];
static char dayofwk[10];
static long juldate;
static int  ithday;

/********************************************************************/
/*				DT_GTOJ.C			    */
/*------------------------------------------------------------------*/
/*	TASK:	Date Gregorian to Julian  (in file DT_DATES.C)      */
/*------------------------------------------------------------------*/
/*	AUTHOR:		J. D. ELBERT                   		    */
/*	DEVELOPED:	3-23-94  				    */
/*	LAST UPDATE:	3-27-94  				    */
/*------------------------------------------------------------------*/
/* 	arguments passed to fcn:   char string MM/DD/YY or YYYY     */
/*      YY implies 19YY; YYYY is entire year, 2001,1888 or 0011     */
/*      indate[] should be 11 characters long to handle all cases   */
/*      returns:  long int (julian date,1 = 1st day AD),-1 if error */
/********************************************************************/

long int dt_gtoj(char indate[])
{
 int acmonthd[] = { 0,31,59,90,120,151,181,212,243,273,304,334,365 };
 long cendays = 36524, yrdays = 365, juldays ;
 int i;
 int leapd=0,leapf, inyr,inday,inmo, centryn, cenyrs, curyr, exday;
 
 /* note on century number: see header */
 if(strlen(indate)==8)         /* YY format */
  { sscanf(indate,"%2d%*1c%2d%*1c%2d",&inmo,&inday,&inyr);   /* parse */
    if(inyr < 0 || inyr > 99 ) return -1;  /* error */
    if(inyr == 0 )   /* YY = 00 , interpreted as 1900 */ 
     { centryn = 18; cenyrs = 99; curyr =100; }
    else
     { centryn = 19; cenyrs = inyr-1; curyr = inyr; }  
  }
 else if(strlen(indate)==10)   /* YYYY format */
  { sscanf(indate,"%2d%*1c%2d%*1c%4d",&inmo,&inday,&inyr);   /* parse */
    if(inyr < 0 ) return -1;  /* error */ 
    centryn = (inyr-1)/100;
    cenyrs = inyr - 1 - centryn*100 ;
    curyr = inyr - centryn*100;   
  } 
 else return -1;  /* wrong format */

 if(inmo < 1 || inmo > 12 ) return -1; /* error in input */
 if(inday< 1) return -1;
 if(curyr%4==0 && (curyr!=0 && curyr!=100 || (centryn+1)%4==0)) leapf=1; /*lyr */
  else leapf =0;
 if(leapf ==1 && inmo==2) exday = 1;
  else exday = 0;
 if(inday >(acmonthd[inmo] - acmonthd[inmo-1] + exday)) return -1; /*err*/


          /* up to but not incl current century */ 
 juldays = (centryn)*cendays;   /* standard days per century, less /400 crit*/
 leapd = (centryn)/4;	/* a leap year caused by /400 criteria */ 

          /* for remaining years this century, but not current yr */  
 juldays += cenyrs * yrdays;
 if(cenyrs != 0) leapd += (cenyrs)/4; /*leap yrs this centry to this yr */ 

        	/* calc total days this year */
 inday += acmonthd[inmo-1];
 if(leapf==1 && inmo > 2) leapd ++;  /* a leap yr & not incl in inday */

                /* Julian days */
 juldays +=  inday + leapd ;  /* Julian Date */
 return (juldays);  /*JULIAN DATE*/
}
